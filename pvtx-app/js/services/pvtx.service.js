import { _getJSON, _postJSON, _putJSON, _delete } from "./api.service";

const PV_URL = `/cgi-bin`;
const PVTX_URL = `${PV_URL}/pvtx`;

export const GetConfig = async () => _getJSON(`${PVTX_URL}/get-config`);

export const SaveUserMeta = async (key = "", value = "") =>
	_postJSON(`${PVTX_URL}/usermeta/save?key=${key}`, value, "text/plain");

export const DeleteUserMeta = async (key = "") =>
	_putJSON(`${PVTX_URL}/usermeta/delete?key=${key}`);

export const Reboot = async (timeout) =>
	_postJSON(`${PVTX_URL}/reboot${timeout ? "?timeout=" + timeout : ""}`);

export const RunGC = async () => _postJSON(`${PVTX_URL}/gc`);

export const Show = async () => _getJSON(`${PVTX_URL}/show`);

export const Begin = async () => _postJSON(`${PVTX_URL}/begin`);

export const Abort = async () => _postJSON(`${PVTX_URL}/abort`);

export const Commit = async () => _postJSON(`${PVTX_URL}/commit`);

export const Add = async (body) =>
	_putJSON(`${PVTX_URL}/add`, body, "application/x-www-form-urlencoded");

export const Remove = async (part) =>
	_putJSON(`${PVTX_URL}/remove${part ? "?part=" + part : ""}`);

export const Status = async (rev) =>
	_getJSON(`${PVTX_URL}/status${rev ? "?rev=" + rev : ""}`);

export const Run = async (rev) =>
	_postJSON(`${PVTX_URL}/run${rev ? "?rev=" + rev : ""}`);

export const Steps = async (rev) =>
	_getJSON(`${PVTX_URL}/steps${rev ? "?rev=" + rev : ""}`);

export const LogsParts = async (rev = "current") =>
	_getJSON(`${PVTX_URL}/logs-parts${rev ? "?rev=" + rev : ""}`);
