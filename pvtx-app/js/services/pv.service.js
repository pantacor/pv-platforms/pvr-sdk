import { _getJSON, _postJSON, _putJSON, _delete } from "./api.service";

const PV_URL = `/cgi-bin`;

export function CancelableFetch(reqInfo, reqInit) {
	var abortController = new AbortController();
	var signal = abortController.signal;
	var cancel = abortController.abort.bind(abortController);

	var wrapResult = function (result) {
		if (result instanceof Promise) {
			var promise = result;
			promise.then = function (onfulfilled, onrejected) {
				var nativeThenResult = Object.getPrototypeOf(this).then.call(
					this,
					onfulfilled,
					onrejected,
				);
				return wrapResult(nativeThenResult);
			};
			promise.cancel = cancel;
		}
		return result;
	};

	var req = window.fetch(reqInfo, Object.assign({ signal: signal }, reqInit));
	return wrapResult(req);
}

export const PropsToQueryString = (props = {}) => {
	return Object.keys(props).reduce((query, key, index) => {
		const separator = index > 0 ? "&" : "?";
		return `${query}${separator}${key}=${props[key]}`;
	}, "");
};

const defaultLogsParams = { follow: true, n: "+0", tail: true, source: ["/"] };

export const GetLogs = (params = {}) =>
	CancelableFetch(
		`${PV_URL}/logs${PropsToQueryString({ ...defaultLogsParams, ...params })}`,
	);

export const GetSteps = () => _getJSON(`${PV_URL}/steps`);
