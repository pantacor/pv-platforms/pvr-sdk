import fetch from "isomorphic-fetch";

/**
 * Executes a promise and returns an array containing either a resolved value or null,
 * along with either null (if successful) or an error object.
 *
 * @param {Promise} promise - The promise to execute.
 * @returns {Promise<[T | null, Error | null]>} An array where the first element is the result of
 *          the promise on success and null on failure, while the second element is null
 *          if the promise was successful or an error object otherwise.
 */
export const safeAwait = async (promise) => {
	try {
		const result = await promise;
		return [result, null];
	} catch (e) {
		return [null, e];
	}
};

export const getError = (json = {}) => {
	if (json.error?.message) {
		return json.error.message;
	}

	if (json.error?.error) {
		return json.error.error;
	}

	if (json.error?.Error) {
		return json.error.Error;
	}

	if (json.Error) {
		return json.Error;
	}

	if (json.error) {
		return json.error;
	}

	return json;
};

export const _requestContentType = async (
	url,
	contentType = "application/json",
	method = "GET",
	body = {},
	token = "",
) => {
	let headers = {
		"Content-Type": contentType,
	};

	if (token) headers["Authorization"] = `Bearer ${token}`;

	let options = {
		method,
		headers: headers,
	};

	if (
		method !== "GET" &&
		method !== "HEAD" &&
		contentType === "application/json"
	) {
		options["body"] = JSON.stringify(body);
	} else if (method !== "GET" && method !== "HEAD") {
		options["body"] = body;
	}

	return await fetch(url, options);
};

const _requestJSON = async (
	url,
	method = "GET",
	body = {},
	type = "application/json",
) => {
	const [response, err] = await safeAwait(
		_requestContentType(url, type, method, body),
	);
	if (err != null) {
		return {
			ok: false,
			redirected: false,
			headers: null,
			status: 500,
			json: {
				error: err,
			},
		};
	}

	if (response.headers.get("Content-Length") === 0) {
		return {
			...response,
			json: null,
		};
	}

	if (!response.ok) {
		return {
			ok: response.ok,
			redirected: response.redirected,
			headers: response.headers,
			status: response.status,
			json: {
				error: new Error("problem with server request"),
			},
		};
	}

	const [json, errJson] = await safeAwait(response.json());
	if (errJson != null) {
		return {
			ok: response.ok,
			redirected: response.redirected,
			headers: response.headers,
			status: response.status,
			json: {
				error: errJson,
			},
		};
	}

	return {
		ok: response.ok,
		redirected: response.redirected,
		headers: response.headers,
		status: response.status,
		json: json,
	};
};

export const _getJSON = async (url) => _requestJSON(url, "GET");

export const _postJSON = async (url, body, type) =>
	_requestJSON(url, "POST", body, type);

export const _putJSON = async (url, body, type) =>
	_requestJSON(url, "PUT", body, type);

export const _patchJSON = async (url, body, type) =>
	_requestJSON(url, "PATCH", body, type);

export const _delete = async (url) => _requestJSON(url, "DELETE");

export async function processService(service, success, failure) {
	let resp;
	try {
		resp = await service();
		if (!resp.ok) {
			failure(
				typeof resp.json.error === "string"
					? { code: resp.status, message: resp.json.error }
					: resp.json,
			);
		} else {
			success(resp.json);
		}
	} catch (err) {
		resp = {
			ok: false,
			json: {
				code: 0,
				message: err,
			},
		};
		failure(resp.json);
	}
	return resp;
}
