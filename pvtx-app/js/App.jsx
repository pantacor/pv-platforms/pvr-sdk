import React from "react";
import {
	MemoryRouter as Router,
	Switch,
	Route,
	NavLink,
} from "react-router-dom";

import RestartAltIcon from "@mui/icons-material/RestartAlt";
import CleaningAltIcon from "@mui/icons-material/CleaningServices";

import logo from "../img/logo.svg";
import logoWhite from "../img/logo-white.svg";
import Main from "./pages/Main/Main";
import ApiReadme from "./pages/ApiReadme/ApiReadme";
import Logs from "./pages/Logs/Logs";
import Metas from "./pages/Metas/Metas";
import { Reboot, RunGC } from "./services/pvtx.service";
import { ConfirmAction } from "./components/ConfirmAction/ConfirmAction";

export default function App() {
	const buildDate = !process.env.BUILD_DATE
		? new Date()
		: new Date(process.env.BUILD_DATE);
	return (
		<Router>
			<header className="header container">
				<div className="row">
					<div className="col-md-6 pt--2 pb--2">
						<img src={logo} height="60" />
					</div>
					<div className="col-md-6 pt--2 pb--2">
						<nav className="nav justify-content-end">
							<NavLink
								activeClassName="active"
								className="nav-link"
								to="/"
								exact={true}
							>
								Home
							</NavLink>
							<NavLink
								activeClassName="active"
								className="nav-link"
								to="/app/config"
							>
								Stats & Config
							</NavLink>
							<NavLink
								activeClassName="active"
								className="nav-link"
								to="/app/api-docs"
							>
								API documentation
							</NavLink>
							<NavLink
								activeClassName="active"
								className="nav-link"
								to="/app/logs"
							>
								Logs
							</NavLink>
							<ConfirmAction
								onConfirm={() => RunGC()}
								title="Run garbage collector"
								confirmBtnTxt="Run garbage collector"
								btnchildren={<CleaningAltIcon />}
							>
								This will execute pantavisor garbage collector,
								are you sure?
							</ConfirmAction>
							<ConfirmAction
								onConfirm={() => Reboot()}
								title="Reboot device"
								confirmBtnTxt="Reboot now"
								btnchildren={<RestartAltIcon />}
							>
								You are going to reboot device, are you sure?
							</ConfirmAction>
						</nav>
					</div>
				</div>
			</header>
			<section className="main container">
				<div
					id="page-content-wrapper"
					className="col-md-12 pt--2 pb--2"
				>
					<Switch>
						<Route path={["/", "/app/"]} exact={true}>
							<Main />
						</Route>
						<Route path="/app/api-docs">
							<ApiReadme />
						</Route>
						<Route path="/app/logs">
							<Logs />
						</Route>
						<Route path="/app/config">
							<Metas />
						</Route>
					</Switch>
				</div>
			</section>
			<footer className="footer container-fluid bg-dark">
				<div className="container">
					<div className="col-md-12 pt--2 pb--2">
						<p>
							<a
								href="https://pantavisor.io"
								target="_blank"
								rel="nofollow"
							>
								<img src={logoWhite} height="30" />
							</a>
						</p>
						<p>
							Pvtx app
							<br />
							Version: {process.env.PACKAGE_VERSION}-
							{process.env.BUILD_VERSION} <br />
							Build Date: {buildDate.toISOString()}
						</p>
					</div>
				</div>
			</footer>
		</Router>
	);
}
