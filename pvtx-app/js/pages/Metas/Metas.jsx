import React, { Component } from 'react'
import EditableMetada from '../../components/EditableMeta/EditableMetada'
import Loading from '../../components/Loading/Loading'
import { Metadata } from '../../components/Metadata/Metada'
import { StorateDetails } from '../../components/StorageDetails/StorageDetails'
import { SystemInfo } from '../../components/SystemInfo/SystemInfo'
import { GetConfig } from '../../services/pvtx.service'

const STATUSES = {
  INITIAL_LOADING: "INITIAL_LOADING",
  LOADING: "LOADING",
  SAVING: "SAVING",
  DONE: "DONE"
}

export default class Metas extends Component {
  state = {
    user: {},
    device: {},
    config: {},
    error: null,
    status: STATUSES.INITIAL_LOADING
  }

  componentDidMount() {
    this.getMeta()  
  }

  getMeta = async () => {
    try {
      const response = await GetConfig()
      if (!response.ok) {
        this.setState({error: response.json, status: STATUSES.DONE})
      }

      this.setState({
        user: response.json.user,
        device: response.json.device,
        config: response.json.config || {},
        error: null,
        status: STATUSES.DONE
      })
    } catch (e) {

    }
  }

  render () {
    if (this.state.status === STATUSES.INITIAL_LOADING) {
      return (
        <div className='metas-page'>
          <Loading />
        </div>
      )
    }
    return (
      <div className='metas-page'>
        <div className="row">
          <div className='col-md-6 pt-4'>
            <SystemInfo className="pl--10 pr--10" info={{...this.state.device.sysinfo}} />
          </div>
          <div className='col-md-6 pt-4'>
            <StorateDetails className="pl--10 pr--10" storage={{...this.state.device.storage}} />
          </div>
        </div>
        <div className='pt-4'>
          <Metadata
            metas={this.state.device}
            type="device-meta"
          />
        </div>
        <div className='pt-4'>
          <EditableMetada metas={this.state.user} />
        </div>
        <div className='pt-4'>
          <Metadata
            metas={this.state.config}
            type="device-config"
          />
        </div>
      </div>
    )
  }
}
