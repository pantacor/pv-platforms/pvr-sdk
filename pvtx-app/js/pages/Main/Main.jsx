import React, { Component } from "react";
import ReactDiffViewer, { DiffMethod } from "react-diff-viewer";
import * as PvtxService from "../../services/pvtx.service";
import DevStatusSummary from "../../components/DevStatusSummary/DevStatusSummary";
import { JSONDisplayWithClipboard } from "../../components/ClipboardFields/ClipboardFields";
import Loading from "../../components/Loading/Loading";
import { Alert } from "../../components/Alert/Alert";
import FileUpload from "../../components/FileUpload/FileUpload";
import { CreateFsDescription } from "../../lib/state";
import { PartDescription } from "../../components/PartDescription/PartDescription";
import { LoadingButton } from "../../components/LoadingButton/LoadingButton";
import { safeAwait } from "../../services/api.service";

const STATUSES = {
	INITIAL_LOAD: "INITIAL_LOAD",
	DEPLOYING: "DEPLOYING",
	UPLOADING: "UPLOADING",
	REMOVING: "REMOVING",
};

const Upload = ({ onFile }) => {
	return (
		<div className="d-flex flex-column align-items-center justify-content-center">
			<FileUpload
				onFile={onFile}
				name="parts"
				type="file"
				message="Drag or click to upload new part"
				pictureStyle={{
					cursor: "pointer",
					width: "400px",
					padding: "1em",
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
					justifyContent: "center",
				}}
				style={{
					backgroundColor: "rgba(0,0,0,0.05)",
					border: "rgba(0,0,0,0.2) 1px solid",
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
					justifyContent: "center",
					padding: "1em",
					marginTop: "0",
					overflow: "hidden",
				}}
			/>
		</div>
	);
};

export default class Main extends Component {
	state = {
		deviceStatus: {
			progress: {
				status: "",
				"status-msg": "",
				progress: 0,
			},
			rev: "0",
		},
		parts: {},
		rev: "",
		json: null,
		transaction: null,
		error: null,
		loading: STATUSES.INITIAL_LOAD,
		deploying: null,
	};

	componentDidMount() {
		this.syncronize();
	}

	syncronize = async () => {
		var [statusRes, err] = await safeAwait(PvtxService.Status());
		if (err != null) {
			console.error(err);
		}

		var [showRes, err] = await safeAwait(PvtxService.Show());
		if (err != null) {
			console.error(err);
		}

		var [stepsRes, err] = await safeAwait(PvtxService.Steps());
		if (err != null) {
			console.error(err);
		}

		const update = { loading: null };
		if (showRes.ok) {
			update.parts = CreateFsDescription(showRes.json);
			update.transaction = showRes.json;
		}

		if (stepsRes.ok) {
			if (update.parts == null) {
				update.parts = CreateFsDescription(stepsRes.json);
			}
			update.json = stepsRes.json;
		} else {
			this.setState({
				...this.state,
				error: "pantavisor offline",
				loading: null,
			});
			return;
		}

		if (statusRes.ok) {
			update.deviceStatus = statusRes.json;
		}

		if (!statusRes.ok) {
			update.error = statusRes.json?.error;
		}

		this.setState(update);
	};

	getError = (json = {}) => {
		if (json.error?.message) {
			return json.error.message;
		}

		if (json.error?.error) {
			return json.error.error;
		}

		if (json.error?.Error) {
			return json.error.Error;
		}

		if (json.Error) {
			return json.Error;
		}

		if (json.error) {
			return json.error;
		}

		return json;
	};

	getStep = async () => {
		const stepsRes = await PvtxService.Steps();
		if (stepsRes.ok) {
			this.setState({
				json: stepsRes.json,
				parts: CreateFsDescription(stepsRes.json),
			});
		}
	};

	begin = async (evnt) => {
		if (this.state.transaction) {
			return;
		}

		this.setState({ transaction: {} });

		const resp = await PvtxService.Begin();
		if (resp.ok) {
			this.setState({
				transaction: resp.json,
				parts: CreateFsDescription(resp.json),
				error: null,
			});
		} else {
			this.setState({
				transaction: null,
				error: this.getError(resp.json),
			});
		}
	};

	remove = (part) => async (evnt) => {
		if (!this.state.parts) {
			return;
		}
		this.setState({ loading: STATUSES.REMOVING });

		const resp = await PvtxService.Remove(part);
		if (resp.ok) {
			this.setState({
				transaction: resp.json,
				parts: CreateFsDescription(resp.json),
				error: null,
				loading: null,
			});
		} else {
			this.setState({ error: this.getError(resp.json), loading: null });
		}
	};

	add = async (bin) => {
		this.setState({ loading: STATUSES.UPLOADING });

		const update = { loading: null };
		const resp = await PvtxService.Add(bin);
		if (resp.ok) {
			update.transaction = resp.json;
			update.error = null;
			update.parts = CreateFsDescription(resp.json);
		} else {
			update.error = this.getError(resp.json);
		}

		update.loading = null;

		this.setState(update);

		return update;
	};

	abort = async () => {
		if (!this.state.parts) {
			return;
		}

		const resp = await PvtxService.Abort();
		if (resp.ok) {
			this.setState({ transaction: null, error: null });
			this.getStep();
		} else {
			this.setState({ error: this.getError(resp.json) });
		}
	};

	commit = async (e) => {
		if (!this.state.parts) {
			return;
		}
		this.setState({ loading: STATUSES.DEPLOYING });

		const resp = await PvtxService.Commit();
		if (!resp.ok) {
			this.setState({ error: this.getError(resp.json), loading: false });
			return;
		}

		if (resp.ok && resp.json?.revision === this.state.deviceStatus?.rev) {
			this.setState({ error: null, loading: false });
			return;
		}

		const respRun = await PvtxService.Run(resp.json.revision);
		if (!respRun.ok) {
			this.setState({ error: respRun.json?.error, loading: false });
			return;
		}

		this.setState({
			transaction: null,
			error: null,
			loading: false,
		});
	};

	onDevicePull = (status) => {
		if (
			status &&
			!status.error &&
			status.rev !== this.state.deviceStatus?.rev
		) {
			this.syncronize();
		}

		if (
			status &&
			!status.error &&
			status.progress?.status !==
				this.state.deviceStatus?.progress?.status
		) {
			this.syncronize();
		}
	};

	render() {
		if (this.state.loading === STATUSES.INITIAL_LOAD) {
			return <Loading />;
		}

		if (this.state.loading === STATUSES.DEPLOYING) {
			return (
				<>
					<DevStatusSummary
						pull={true}
						initialStatus={this.state.deviceStatus}
						rev={this.state.rev}
						onUpdate={this.onDevicePull}
					/>
					<div className="d-flex flex-column align-items-center justify-content-center pt--4 pb--4">
						<h1>Deploying changes...</h1>
						<Loading />
					</div>
				</>
			);
		}

		return (
			<div>
				{this.state.error && (
					<Alert className="alert-danger">{this.state.error}</Alert>
				)}
				<DevStatusSummary
					pull={true}
					initialStatus={this.state.deviceStatus}
					rev={this.state.rev}
					onUpdate={this.onDevicePull}
				/>
				{!this.state.transaction && (
					<>
						<div className="d-flex justify-content-center pt--4 pb--4">
							<LoadingButton
								onClick={this.begin}
								className="btn btn-primary btn-lg d-flex  align-items-center"
								disabled={
									this.state.loading === STATUSES.DEPLOYING
								}
							>
								Begin new transaction
							</LoadingButton>
						</div>
						<div className="pb--4">
							{Object.keys(this.state.parts).map((key) => (
								<PartDescription
									key={key}
									title={key}
									desc={this.state.parts[key]}
									onDelete={null}
								/>
							))}
						</div>
					</>
				)}
				{this.state.transaction && (
					<>
						<div className="pb--4 pt--4">
							<Upload onFile={this.add} />
						</div>
						{Object.keys(this.state.parts).map((key) => (
							<PartDescription
								key={key}
								title={key}
								desc={this.state.parts[key]}
								onDelete={this.remove}
							/>
						))}
						{JSON.stringify(this.state.json) !==
							JSON.stringify(this.state.transaction) && (
							<div className="pt--2 pb--2">
								<h4 className="pt--2 pb--2">
									Transaction diff
								</h4>
								<div className="row">
									<div className="col-md-12 overflow-scroll">
										<ReactDiffViewer
											compareMethod={DiffMethod.WORDS}
											oldValue={JSON.stringify(
												this.state.json,
												"",
												"  ",
											)}
											newValue={JSON.stringify(
												this.state.transaction,
												"",
												"  ",
											)}
											splitView={false}
										/>
									</div>
								</div>
							</div>
						)}

						<div className="d-flex justify-content-center pt--4 pb--4">
							<LoadingButton
								disabled={
									this.state.loading === STATUSES.DEPLOYING ||
									this.state.loading === STATUSES.REMOVING ||
									this.state.loading === STATUSES.UPLOADING
								}
								onClick={this.abort}
								className="btn btn-secondary ml--1 mr--1 d-flex align-items-center"
							>
								Abort transaction
							</LoadingButton>
							<LoadingButton
								disabled={
									this.state.loading === STATUSES.DEPLOYING ||
									this.state.loading === STATUSES.REMOVING ||
									this.state.loading === STATUSES.UPLOADING
								}
								onClick={this.commit}
								className="btn btn-primary ml--1 mr--1 d-flex align-items-center"
							>
								Commit transaction
							</LoadingButton>
						</div>
					</>
				)}
			</div>
		);
	}
}
