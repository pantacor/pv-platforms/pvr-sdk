import React, { Component } from "react";
import * as PvService from "../../services/pv.service";
import LogsPrint from "../../components/LogsPrint/LogsPrint";
import LogsFilter from "../../components/LogsFilter/LogsFilter";

export default class Logs extends Component {
	state = {
		source: ["/"],
		rev: "current",
	};

	onChange = (props) => {
		this.setState(props);
	};

	render() {
		return (
			<div>
				<header>
					<LogsFilter
						OnUpdate={this.onChange}
						source={this.state.source}
						rev={this.state.rev}
					/>
				</header>
				<LogsPrint source={this.state.source} rev={this.state.rev} />
			</div>
		);
	}
}
