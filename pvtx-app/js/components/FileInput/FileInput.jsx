import React from 'react'

function FileInput (props, ref) {
  const {
    name,
    label,
    onInput = () => {},
    onChange = () => {},
    error,
    type = 'file',
    ...rest
  } = props

  if (!label) {
    return (
      <React.Fragment>
        <input
          ref={ref}
          type={type}
          id={name}
          name={name}
          onInput={onInput}
          onChange={onChange}
          {...rest}
        />
        <div className="invalid-feedback"> {error} </div>
      </React.Fragment>
    )
  }

  return (
    <div className="form-group pb-2">
      <label htmlFor={name}>{label}</label>
      <input
        ref={ref}
        type={type}
        id={name}
        name={name}
        onInput={onInput}
        onChange={onChange}
        {...rest}
      />
      <div className="invalid-feedback"> {error} </div>
    </div>
  )
}

export default React.forwardRef(FileInput)
