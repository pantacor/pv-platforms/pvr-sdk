import React from "react";

export function Button(props) {
	const { children, ...extra } = props;
	return <button {...extra}>{children}</button>;
}
