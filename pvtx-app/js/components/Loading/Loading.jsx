import React from "react";

export default function Loading({ extraClass = "" }) {
	return (
		<div className="d-flex justify-content-center">
			<div className={`spinner-border ${extraClass}`} role="status">
				<span className="visually-hidden">Loading...</span>
			</div>
		</div>
	);
}
