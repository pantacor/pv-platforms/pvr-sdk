import { useState } from "react";
import Loading from "../Loading/Loading";

export function LoadingButton({ onClick, children, ...extraprops }) {
	const [loading, setLoading] = useState(false);
	const { className = "btn btn-primary btn-lg", ...extra } = extraprops;

	const onClickHandler = (event) => {
		event.preventDefault();

		if (!loading) {
			setLoading(true);
			onClick(event)
				.then(() => setLoading(false))
				.catch(() => setLoading(false));
		}
	};
	return (
		<button
			onClick={onClickHandler}
			disabled={loading}
			className={className}
			{...extra}
		>
			{children}
			{loading && <Loading extraClass="spinner-border-sm ml--10" />}
		</button>
	);
}
