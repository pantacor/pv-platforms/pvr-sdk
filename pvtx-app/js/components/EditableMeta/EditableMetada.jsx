import React, { Component } from "react";
import {
	DeleteUserMeta,
	SaveUserMeta,
	SetUserMeta,
} from "../../services/pvtx.service";
import { EditableMeta } from "./EditableMeta";

export default class EditableMetada extends Component {
	state = {
		metas: {},
		editedMeta: null,
		editing: false,
		newKey: "",
		newValue: "",
		loading: "",
		globalMeta: {},
		err: "",
	};

	componentDidMount() {
		this.setState({ metas: this.props.metas });
	}

	title(type) {
		return type ? capitalize(type.replace("-", " ")) : undefined;
	}

	meta = () => {
		const { metas } = this.state;

		return this.mapMetas(metas);
	};

	mapMetas = (metas) => {
		return Object.keys(metas).reduce((acc, key) => {
			const type = "user";
			acc[key] = {
				type: type,
				value: metas[key],
			};
			return acc;
		}, {});
	};

	saveHandler = async (oldKey, newKey, oldValue, newValue) => {
		const fullMeta = {
			...this.meta(),
			...(this.state.editedMeta || {}),
		};
		delete fullMeta[oldKey];
		this.setState({
			editedMeta: { ...fullMeta },
			editing: false,
		});
		const type = "user";
		fullMeta[newKey] = { type, value: newValue };

		const resp = await SaveUserMeta(newKey, newValue);
		if (resp.ok) {
			this.setState({ metas: resp.json, editedMeta: fullMeta });
		}
	};

	editHandler = (edit) => {
		this.setState({
			editing: edit,
		});
	};

	deleteHandler = async (key) => {
		const fullMeta = {
			...this.meta(),
			...(this.state.editedMeta || {}),
		};
		if (this.state.globalMeta[key]) {
			fullMeta[key] = { type: "global", value: null };
		} else {
			delete fullMeta[key];
		}
		this.setState({
			editedMeta: fullMeta,
		});
		const resp = await DeleteUserMeta(key);
		if (resp.ok) {
			this.setState({ metas: resp.json, editedMeta: fullMeta });
		}
	};

	addHandler = async () => {
		if (this.state.newKey === "" || this.state.newValue === "") {
			this.setState({ err: "new user meta can't be empty" });
			return;
		}
		const fullMeta = {
			...this.meta(),
			...(this.state.editedMeta || {}),
		};
		const editedMeta = {
			...fullMeta,
			[this.state.newKey]: { type: "user", value: this.state.newValue },
		};
		this.setState({
			newKey: "",
			newValue: "",
			editedMeta: editedMeta,
		});
		const resp = await SaveUserMeta(this.state.newKey, this.state.newValue);
		if (resp.ok) {
			this.setState({
				metas: resp.json,
				editedMeta: this.mapMetas(resp.json),
			});
		}
	};

	getApiMeta = (meta) => {
		return Object.keys(meta).reduce((acc, key) => {
			acc[key] = meta[key].value;

			return acc;
		}, {});
	};

	newValueHandler = (evt) => this.setState({ newValue: evt.target.value });
	newKeyHandler = (evt) => this.setState({ newKey: evt.target.value });

	render() {
		const { type } = this.props;
		const { newKey, newValue, editing, loading } = this.state;

		return (
			<EditableMeta
				title={this.title(type)}
				meta={this.meta()}
				globalMeta={this.state.globalMeta}
				editedMeta={this.state.editedMeta || this.meta()}
				loading={loading}
				editing={editing}
				newKey={newKey}
				newValue={newValue}
				err={this.state.err}
				saveHandler={this.saveHandler}
				editHandler={this.editHandler}
				deleteHandler={this.deleteHandler}
				addHandler={this.addHandler}
				newKeyHandler={this.newKeyHandler}
				newValueHandler={this.newValueHandler}
			/>
		);
	}
}
