import React, { Fragment } from "react";
import MetadataRow from "../MetadataRow/MetadataRow";
import { Button } from "../Button/Button";
import SaveIcon from "@mui/icons-material/Save";

export function EditableMeta(props) {
	const {
		title,
		meta,
		editedMeta,
		loading,
		editing,
		newKey,
		newValue,
		globalMeta,
		err = "",
		saveHandler = () => {},
		editHandler = () => {},
		deleteHandler = () => {},
		addHandler = () => {},
		newKeyHandler = () => {},
		newValueHandler = () => {},
	} = props;

	const mergedMeta = {
		...(meta || {}),
		...(editedMeta || {}),
	};
	const metaEntries = Object.entries(mergedMeta).sort((a, b) => {
		if (a[1].type && b[1].type) {
			return a[1].type > b[1].type;
		}
		if (a[0] > b[0]) {
			return 1;
		}
		if (a[0] < b[0]) {
			return -1;
		}
		// a must be equal to b
		return 0;
	});
	const metaEntriesCount = metaEntries.length;

	return (
		<div className="device-meta">
			{title && (
				<h3 className="mb-3 meta-title font-weight-light">{title}</h3>
			)}
			<div className="table-responsive">
				<table className="table table-borderless table-hover table-responsive table-lg">
					<thead>
						<tr>
							<th>Key</th>
							<th>Value</th>
							<th />
						</tr>
					</thead>
					<tbody>
						{metaEntries.map(([key, value]) => {
							const inProgress =
								(editedMeta &&
									!!editedMeta[key] &&
									!meta[key]) ||
								(editedMeta && !editedMeta[key] && !!meta[key]);
							return (
								<MetadataRow
									key={key}
									inProgress={inProgress}
									globalMeta={globalMeta}
									currentKey={key}
									type={value.type}
									value={value.value ? value.value : value}
									loading={loading}
									disabled={editing === true}
									onEdit={editHandler}
									saveHandler={saveHandler}
									onDelete={deleteHandler}
								/>
							);
						})}
						{metaEntriesCount === 0 && (
							<tr className="bg-light">
								<th className="text-center" colSpan={3}>
									Metadata is empty
								</th>
							</tr>
						)}
						<tr className="new-entry-header">
							<td colSpan={3}>
								<label>Add a new entry</label>
							</td>
						</tr>
						<tr className="new-entry-form">
							<td colSpan={1}>
								<input
									type="text"
									className="form-control my-1 mr-sm-2"
									disabled={loading}
									aria-label="Metadata Field Label"
									onChange={newKeyHandler}
									value={newKey}
									placeholder="key"
								/>
							</td>
							<td colSpan={1}>
								<Fragment>
									<textarea
										className="form-control my-1 mr-sm-2 textarea"
										disabled={loading}
										aria-label="Metadata Field Value"
										placeholder="value"
										onChange={newValueHandler}
										rows={Math.max(
											newValue.split("\n").length,
											1,
										)}
										value={newValue}
									/>
									{err !== "" && (
										<div
											className="alert alert-danger"
											role="alert"
										>
											{err}
										</div>
									)}
								</Fragment>
							</td>
							<td className="pt-2 text-end">
								<Button
									type="button"
									className={`btn btn-primary btn-sm my-1 mr-sm-2 ${
										loading ? "disabled" : ""
									}`}
									disabled={loading}
									onClick={addHandler}
									title="Add"
								>
									<SaveIcon />
								</Button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	);
}
