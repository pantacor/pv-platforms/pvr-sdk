import React, { useState, memo } from "react";
import { resolvePath } from "../../lib/utils";
import { Button } from "../Button/Button";
import HourglassBottomIcon from "@mui/icons-material/HourglassBottom";
import CancelIcon from "@mui/icons-material/Cancel";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import SaveIcon from "@mui/icons-material/Save";

export default memo(function MetadataRow(props) {
	const [state, setState] = useState({
		inEdit: false,
		newKey: props.currentKey,
		newValue: props.value || "",
	});
	const {
		inProgress,
		loading,
		disabled,
		currentKey: key,
		onEdit,
		saveHandler,
		onDelete,
		type,
		globalMeta = {},
	} = props;
	const { inEdit, newKey, newValue } = state;
	const value = resolvePath(
		props,
		"value.value",
		resolvePath(props, "value"),
	);

	const updateKey = (evt) => {
		setState({
			...state,
			newKey: evt.target.value,
		});
	};

	const updateValue = (evt) => {
		setState({
			...state,
			newValue: evt.target.value,
		});
	};
	return (
		<tr
			className={`${
				inProgress || (disabled && !inEdit) ? "table-secondary" : ""
			} ${inEdit ? "metadata-edit new-entry-form" : ""}`}
			title={`${inProgress ? "Operation in progress" : ""}`}
		>
			<td className="meta-key">
				{inEdit ? (
					<input
						className="form-control my-1 mr-sm-2"
						type="text"
						placeholder={key}
						value={newKey}
						onChange={updateKey}
					/>
				) : (
					<pre
						style={{
							textDecoration:
								globalMeta[key] && value === null
									? "line-through"
									: "none",
						}}
					>{`${key}`}</pre>
				)}
			</td>
			<td style={{ maxWidth: "50vw" }}>
				{inEdit ? (
					<textarea
						className="form-control my-1 mr-sm-2 textarea"
						aria-label="Metadata Field Value"
						placeholder={value}
						value={newValue}
						rows={Math.max(newValue.split("\n").length, 1)}
						onChange={updateValue}
					/>
				) : (
					<pre
						style={{
							textDecoration:
								globalMeta[key] && value === null
									? "line-through"
									: "none",
						}}
					>{`${globalMeta[key] && value === null ? globalMeta[key] : value}`}</pre>
				)}
			</td>
			<td className="actions text-end" width="150">
				<div className="btn-group my-1 mr-sm-2">
					{inEdit ? (
						<React.Fragment>
							<Button
								href={undefined}
								style={{ color: "white" }}
								className={`btn btn-sm btn-light ${
									inProgress || loading ? "disabled" : ""
								}`}
								disabled={inProgress || loading}
								title="Cancel"
								onClick={() => {
									setState({
										...state,
										inEdit: false,
										newKey: props.key,
										newValue: props.value,
									});
									onEdit(false);
								}}
							>
								{inProgress ? (
									<HourglassBottomIcon />
								) : (
									<CancelIcon />
								)}
							</Button>
							<Button
								style={{ color: "white" }}
								className={`btn btn-sm btn-primary ${
									inProgress || loading ? "disabled" : ""
								}`}
								disabled={inProgress || loading}
								title="Save"
								onClick={() => {
									saveHandler(
										key,
										newKey,
										value,
										newValue,
										type,
									);
									setState({
										...state,
										inEdit: false,
									});
								}}
							>
								{inProgress ? (
									<HourglassBottomIcon />
								) : (
									<SaveIcon />
								)}
							</Button>
						</React.Fragment>
					) : (
						<React.Fragment>
							{globalMeta[key] && globalMeta[key] !== value && (
								<Button
									style={{ color: "white" }}
									className={`btn btn-sm btn-light ${
										inProgress || loading || disabled
											? "disabled"
											: ""
									}`}
									disabled={inProgress || loading || disabled}
									title="Restore to global value"
									onClick={() => {
										setState({
											...state,
											newValue: globalMeta[key],
											newKey: key,
											inEdit: true,
										});
										onEdit(true);
									}}
								>
									{inProgress ? (
										<HourglassBottomIcon />
									) : (
										<CancelIcon />
									)}
								</Button>
							)}
							<Button
								style={{ color: "white" }}
								className={`btn btn-sm btn-info ${
									inProgress || loading || disabled
										? "disabled"
										: ""
								}`}
								disabled={inProgress || loading || disabled}
								title="Edit this key"
								onClick={() => {
									setState({
										...state,
										inEdit: true,
									});
									onEdit(true);
								}}
							>
								{inProgress ? (
									<HourglassBottomIcon />
								) : (
									<EditIcon />
								)}
							</Button>
						</React.Fragment>
					)}
					<Button
						style={{ color: "white" }}
						className={`btn btn-sm btn-danger ${
							inProgress || loading || disabled ? "disabled" : ""
						}`}
						disabled={inProgress || loading || disabled}
						title="Remove this key from metadata"
						onClick={() => {
							onDelete(key, type);
						}}
					>
						{inProgress ? <HourglassBottomIcon /> : <DeleteIcon />}
					</Button>
				</div>
			</td>
		</tr>
	);
});
