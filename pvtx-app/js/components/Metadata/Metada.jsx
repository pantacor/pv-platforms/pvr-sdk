import React from "react";
import { capitalize, flatObject, GetFileSize } from "../../lib/utils";

export function MetadaRow(props) {
	let { currentKey: key, value, convertStorage = false } = props;

	if (!value) {
		return null;
	}

	if (convertStorage && key.indexOf("storage.") >= 0 && !!value) {
		value = GetFileSize(value);
	}

	return (
		<tr>
			<td className="meta-key">
				<pre>{key}</pre>
			</td>
			<td>
				<pre>{!value ? "null" : value.toString()}</pre>
			</td>
		</tr>
	);
}

export function Metadata(props) {
	const { metas = {}, type = "user-meta" } = props;
	// const loading = navigator.setMeta.loading;
	const title = capitalize(type.replace("-", " "));
	const flattenMeta = flatObject(metas);
	const metaEntries = Object.entries(flattenMeta).sort((a, b) => {
		if (a[0] > b[0]) {
			return 1;
		}
		if (a[0] < b[0]) {
			return -1;
		}
		// a must be equal to b
		return 0;
	});
	const metaEntriesCount = metaEntries.length;

	return (
		<div className="device-meta">
			<h3 className="meta-title">{title}</h3>
			<div className="table-responsive">
				<table className="table table-borderless table-hover table-responsive table-lg">
					<thead>
						<tr>
							<th>Key</th>
							<th>Value</th>
							<th />
						</tr>
					</thead>
					<tbody>
						{metaEntries.map(([key, value]) => {
							return (
								<MetadaRow
									convertStorage={true}
									key={key}
									currentKey={key}
									value={value}
								/>
							);
						})}
						{metaEntriesCount === 0 && (
							<tr className="bg-light">
								<th className="text-center" colSpan={3}>
									Metadata is empty
								</th>
							</tr>
						)}
					</tbody>
				</table>
			</div>
		</div>
	);
}
