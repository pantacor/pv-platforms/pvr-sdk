import React from "react";
import { Usage } from "../Progress/Progress";

export function SystemInfo({ info, ...extras }) {
	return (
		<div {...extras}>
			<section>
				<h6>Ram</h6>
				<Usage
					value={info.totalram - info.freeram}
					max={info.totalram}
				/>
			</section>
			<section className="pt-4">
				<h6>Swap</h6>
				<Usage
					value={info.totalswap - info.freeswap}
					max={info.totalswap}
				/>
			</section>
		</div>
	);
}
