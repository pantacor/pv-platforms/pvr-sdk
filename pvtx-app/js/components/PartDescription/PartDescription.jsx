import { Fragment, useState } from "react";
import WidgetsIcon from "@mui/icons-material/Widgets";
import FolderIcon from "@mui/icons-material/Folder";
import DataObjectIcon from "@mui/icons-material/DataObject";
import ShieldIcon from "@mui/icons-material/Shield";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import { TYPES } from "../../lib/state";
import { JSONDisplayWithClipboard } from "../ClipboardFields/ClipboardFields";
import { ConfirmAction } from "../ConfirmAction/ConfirmAction";

import "./partdescription.scss";

const mergeContentDescription = (desc = {}) => {
	if ([TYPES.JSON, TYPES.OBJECT].some((v) => v == desc.type)) {
		return desc.content;
	}

	return Object.keys(desc.content).reduce((acc, key = "") => {
		const d = desc.content[key];
		if (d.type === TYPES.FOLDER) {
			return { ...acc, ...mergeContentDescription(d) };
		}

		switch (d.type) {
			case TYPES.FOLDER:
				return { ...acc, ...mergeContentDescription(d) };
			case TYPES.SIGNATURE:
				return { ...acc, [d.id]: d.raw };
			default:
				return { ...acc, [d.id]: d.content };
		}
	}, {});
};

function AcordionItem({ title, name, onDelete, children }) {
	const [show, setShow] = useState(false);
	const [loading, setLoading] = useState(false);

	const toggleShow = (evnt) => {
		evnt.preventDefault();
		setShow(!show);
	};

	const deleteHandle = async () => {
		setLoading(true);
		try {
			await onDelete();
		} catch (e) {
			console.error(e);
		}
		setLoading(false);
	};

	return (
		<div className="accordion-item">
			<div className="accordion-header">
				<button
					className={`accordion-button ${!show ? "collapsed" : ""}`}
					type="button"
					onClick={toggleShow}
				>
					{typeof title === "string" ? title : title()}
				</button>
				{onDelete && (
					<ConfirmAction
						title={`Delete ${name || title}`}
						confirmBtnTxt="Delete this"
						onConfirm={deleteHandle}
						loading={loading}
						btnClassName={`btn btn-sm btn-light btn-link ${!show ? "collapsed" : ""}`}
					>
						You are going to delete <b>{name || title}</b> from your
						device.
					</ConfirmAction>
				)}
			</div>
			<div
				className={`accordion-collapse collapse ${show ? "show" : ""}`}
			>
				<div className="accordion-body">{children}</div>
			</div>
		</div>
	);
}

function TitleIcon({ type }) {
	switch (type) {
		case TYPES.FOLDER:
			return <FolderIcon />;
		case TYPES.JSON:
			return <DataObjectIcon />;
		case TYPES.OBJECT:
			return <AttachFileIcon />;
		case TYPES.SIGNATURE:
			return <ShieldIcon />;
		case TYPES.PACKAGE:
			return <WidgetsIcon />;
		default:
			return null;
	}
}
function Title({ desc, title }) {
	return (
		<>
			<span style={{ paddingRight: "10px" }}>
				<TitleIcon type={desc.type} />
			</span>
			{title || desc.id}
		</>
	);
}

function Part({ title, desc, onDelete }) {
	if (!onDelete) {
		onDelete = () => null;
	}
	return (
		<AcordionItem
			onDelete={onDelete(desc.id)}
			name={desc.id}
			title={() => <Title desc={desc} title={title} />}
		>
			<JSONDisplayWithClipboard value={mergeContentDescription(desc)} />
		</AcordionItem>
	);
}

function Parts({ desc, onDelete }) {
	if (desc.type === TYPES.FOLDER) {
		return <Folder desc={desc} onDelete={onDelete} />;
	}

	return <Part title={desc.id} desc={desc} onDelete={onDelete} />;
}

export function Folder({ desc, onDelete }) {
	if (!onDelete) {
		onDelete = () => null;
	}
	return (
		<div className="part-description accordion">
			{Object.entries(desc.content).map(([key, value]) => (
				<Fragment key={key}>
					{[TYPES.JSON, TYPES.OBJECT].some((v) => v == value.type) ? (
						<Parts desc={value} onDelete={onDelete} />
					) : (
						<AcordionItem
							onDelete={onDelete(value.id)}
							name={value.id}
							title={() => (
								<Title desc={value} title={value.id} />
							)}
						>
							<Parts desc={value} onDelete={onDelete} />
						</AcordionItem>
					)}
				</Fragment>
			))}
		</div>
	);
}

export function Package({ desc, title, onDelete }) {
	if (!onDelete) {
		onDelete = () => null;
	}
	return (
		<AcordionItem
			onDelete={onDelete(desc.id)}
			name={desc.id}
			title={() => <Title desc={desc} title={title} />}
		>
			<JSONDisplayWithClipboard value={mergeContentDescription(desc)} />
		</AcordionItem>
	);
}

function PartByType(props) {
	switch (props.desc.type) {
		case TYPES.PACKAGE:
			return <Package {...props} />;
		case TYPES.FOLDER:
			return <Folder {...props} />;
		default:
			return <Part {...props} />;
	}
}
export function PartDescription(props) {
	return (
		<div className="part-description accordion">
			<PartByType {...props} />
		</div>
	);
}
