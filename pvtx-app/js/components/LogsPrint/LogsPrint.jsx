import React, { Component } from "react";
import * as PvService from "../../services/pv.service";
import FullscreenIcon from "@mui/icons-material/Fullscreen";
import VerticalAlignBottomIcon from "@mui/icons-material/VerticalAlignBottom";

export default class LogsPrint extends Component {
	state = {
		text: "",
		follow: true,
		stickToBottom: true,
	};

	request = null;

	constructor(props) {
		super(props);

		this.reader = null;
		this.decoder = new TextDecoder();
		this.preRef = React.createRef();
	}

	componentDidMount() {
		this.closeReader();
		this.getLogs({ source: this.props.source, rev: this.props.rev });
		if (this.preRef.current) {
			const wrapper = this.preRef.current;
			wrapper.onwheel = this.onWheel;
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			prevProps.source !== this.props.source ||
			prevProps.rev !== this.props.rev
		) {
			this.closeReader();
			this.getLogs({ source: this.props.source, rev: this.props.rev });
		}
		if (this.preRef.current && this.state.stickToBottom) {
			const wrapper = this.preRef.current;
			wrapper.scrollTop = wrapper.scrollHeight;
		}
	}

	componentWillUnmount() {
		this.closeReader();
	}

	onWheel = (event) => {
		if (!this.preRef) {
			return;
		}
		const wrapper = this.preRef.current;
		if (wrapper && wrapper.clientHeight > 0) {
			if (this.state.stickToBottom) {
				this.setState({ stickToBottom: false });
			}
		}
	};

	saveToState = (text) => {
		this.setState({ text: `${this.state.text}${text}` });
	};

	processReader = (response) => {
		this.reader = response.body.getReader();
		return this.reader.read().then(this.processText);
	};

	processText = ({ done, value }) => {
		if (done) {
			return;
		}
		this.saveToState(this.decoder.decode(value));
		return this.reader.read().then(this.processText);
	};

	getLogs = (args) => {
		if (!this.reader) {
			this.request = PvService.GetLogs(args).then(this.processReader);
		}
	};

	closeReader = () => {
		if (this.request) {
			this.request.cancel();
			this.request = null;
		}
		if (this.reader) {
			this.reader.cancel();
			this.reader = null;
			this.setState({ text: "" });
		}
	};

	stickToBottom = () => {
		this.setState({ stickToBottom: true });
	};

	makeFullscreen = () => {
		if (this.preRef.current) {
			this.preRef.current.requestFullscreen();
		}
	};

	render() {
		return (
			<div>
				<pre
					ref={this.preRef}
					className="main"
					style={{
						height: "90vh",
						overflow: "auto",
						backgroundColor: "black",
						color: "white",
						padding: "1em",
						marginTop: "2em",
					}}
				>
					{this.state.text}
				</pre>
				<div className="d-grid gap-2 d-md-flex justify-content-md-end">
					<button
						title="Auto follow end of logs"
						className="btn btn-primary btn-sm"
						onClick={this.stickToBottom}
					>
						<VerticalAlignBottomIcon />
					</button>
					<button
						title="Make logs fullscreen"
						className="btn btn-primary btn-sm"
						onClick={this.makeFullscreen}
					>
						<FullscreenIcon />
					</button>
				</div>
			</div>
		);
	}
}
