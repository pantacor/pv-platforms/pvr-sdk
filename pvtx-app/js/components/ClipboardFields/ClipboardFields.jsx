import React, { useEffect, useRef } from "react";

import { CopyToClipboard } from "react-copy-to-clipboard";
import { Button } from "../Button/Button";
import CopyAll from "@mui/icons-material/CopyAll";

export const CopyButton = (props) => (
	<CopyToClipboard text={props.value}>
		<Button
			className={`clipboard-button ${props.className}`}
			title={props.title}
			type="button"
			disabled={props.disabled}
		>
			{props.children}
		</Button>
	</CopyToClipboard>
);

export const TextInputWithClipboard = (props) => (
	<div className="input-group input-group-sm">
		<input
			type="text"
			className="form-control form-control-sm"
			value={props.value}
			aria-label={props.label}
			disabled
		/>
		<span className="input-group-append">
			<CopyButton
				className="btn btn-sm btn-light"
				value={props.value}
				title={props.title || "Copy Value"}
				disabled={props.disabled}
			>
				<CopyAll />
			</CopyButton>
		</span>
	</div>
);

export const JSONDisplayWithClipboard = (props) => {
	const jsonStr = JSON.stringify(
		props.value,
		(_key, value) => (value instanceof Set ? [...value] : value),
		2,
	);

	const ref = useRef();
	useEffect(() => {
		if (ref.current && !ref.current.classList.contains("hljs")) {
			if (window.hljs) {
				window.hljs.highlightAll();
				window.hljs.initLineNumbersOnLoad();
			}
		}
	}, [ref, ref.current, window.hljs]);

	return (
		<div className="json-with-clipboard">
			<CopyButton
				className="btn btn-sm btn-light"
				value={jsonStr}
				title={props.title || "Copy JSON content"}
				disabled={props.disabled}
			>
				<CopyAll />
			</CopyButton>
			<div className="json-value-wrapper js-syntax-highlight dark">
				<pre>
					<code ref={ref} className="language-json">
						{jsonStr}
					</code>
				</pre>
			</div>
		</div>
	);
};

export const TextDisplayWithClipboard = ({
	value,
	type = "text",
	disabled,
	title,
}) => {
	const ref = useRef();
	useEffect(() => {
		if (ref.current && !ref.current.classList.contains("hljs")) {
			window.hljs.highlightAll();
			window.hljs.initLineNumbersOnLoad();
		}
	}, [ref, ref.current]);

	return (
		<div className="json-with-clipboard">
			<CopyButton
				className="btn btn-sm btn-light"
				value={value}
				title={title || "Copy content"}
				disabled={disabled}
			>
				<CopyAll />
			</CopyButton>
			<div className="json-value-wrapper js-syntax-highlight dark">
				<pre>
					<code ref={ref} className={`language-${type}`}>
						{value}
					</code>
				</pre>
			</div>
		</div>
	);
};
