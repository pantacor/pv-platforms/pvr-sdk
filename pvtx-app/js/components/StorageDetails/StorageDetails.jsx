import React from "react";
import { Usage } from "../Progress/Progress";

export function StorateDetails({ storage, ...extras }) {
	return (
		<div {...extras}>
			<section>
				<h6>Disk usage</h6>
				<Usage
					value={storage.total - storage.real_free}
					max={storage.total}
				/>
			</section>
			<section className="pt-4">
				<h6>Reserved</h6>
				<Usage
					value={storage.total - storage.reserved}
					max={storage.total}
				/>
			</section>
		</div>
	);
}
