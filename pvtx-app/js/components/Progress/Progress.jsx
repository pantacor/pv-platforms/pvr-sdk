import React from "react";
import { GetFileSize } from "../../lib/utils";

export function Usage({ value, max }) {
	const percentage = (value * 100) / max;
	return (
		<div className="progress" style={{ height: "20px" }}>
			<div
				className="progress-bar"
				role="progressbar"
				aria-valuenow={GetFileSize(value).toString()}
				aria-valuemin="0"
				aria-valuemax={GetFileSize(max).toString()}
				style={{ width: `${percentage}%` }}
			>
				{percentage > 50 && (
					<>
						{GetFileSize(value).toString()}/
						{GetFileSize(max).toString()}
					</>
				)}
			</div>
			{percentage < 50 && (
				<div
					style={{
						textAlign: "center",
						width: `${100 - percentage}%`,
					}}
				>
					{GetFileSize(value).toString()}/
					{GetFileSize(max).toString()}
				</div>
			)}
		</div>
	);
}
