import React from "react";
import { PureComponent } from "react";
import { Status } from "../../services/pvtx.service";
import { safeAwait } from "../../services/api.service";

const emptyFunction = () => {};

export default class DevStatusSummary extends PureComponent {
	state = {
		rev: "",
		progress: {
			progress: "",
			"status-msg": "",
			progress: 0,
		},
	};

	static getDerivedStateFromProps(props, state) {
		if (state.rev === "") {
			return {
				...state,
				...props.initialStatus,
			};
		}

		return state;
	}

	constructor(props) {
		super(props);

		this.interval = null;
	}

	componentDidMount() {
		this.props.pull ? this.getStatus(0) : this.stopPulling();
	}

	componentDidUpdate() {
		const updateCb = this.props.onUpdate || emptyFunction;
		updateCb(this.state);
		this.props.pull ? this.getStatus() : this.stopPulling();
	}

	componentWillUnmount() {
		this.stopPulling();
	}

	getStatus = (start = 3000) => {
		this.interval = setTimeout(async () => {
			const [statusRes, err] = await safeAwait(Status());
			if (err != null) {
				console.error(err);
				return;
			}
			if (statusRes.ok) {
				this.setState(statusRes.json);
			}
		}, start);
	};

	stopPulling = () => {
		if (!this.interval) {
			return;
		}
		clearTimeout(this.interval);
		this.interval = null;
	};

	render() {
		return (
			<section className="row">
				<div className="col-md-4">
					<div className="">
						<span>Rev: </span>
						<span style={{ fontWeight: "bold" }}>
							{this.state.rev}
						</span>
					</div>
					<div className="">
						<span>Status: </span>
						<span style={{ fontWeight: "bold" }}>
							{this.state.progress.status}
						</span>
					</div>
					<div className="">
						<span>Progress: </span>
						<span style={{ fontWeight: "bold" }}>
							{this.state.progress.progress}
						</span>
					</div>
					{this.state.progress["status-msg"] &&
						this.state.progress["status-msg"] !== "" && (
							<div className="">
								<span>{this.state.progress["status-msg"]}</span>
							</div>
						)}
				</div>
			</section>
		);
	}
}
