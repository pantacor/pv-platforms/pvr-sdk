import React, { Component } from "react";
import * as PvService from "../../services/pv.service";
import { LogsParts } from "../../services/pvtx.service";
import Loading from "../Loading/Loading";

export const LOGS_FILTER_STATUS = {
	LOADING: "loading",
};
export default class LogsFilter extends Component {
	state = {
		revisions: [],
		parts: [],
		rev: "current",
		source: ["/"],
		status: LOGS_FILTER_STATUS.LOADING,
	};

	async componentDidMount() {
		await PvService.GetSteps().then(this.getRevisions);
		await this.getSources(this.state.rev);
		this.setState({ status: null });
	}

	componentDidUpdate() {
		if (
			this.props.OnUpdate &&
			(this.props.source !== this.state.source ||
				this.props.rev !== this.state.rev)
		) {
			this.props.OnUpdate({
				source: this.state.source,
				rev: this.state.rev,
			});
		}
	}

	getRevisions = (response) => {
		if (response.ok) {
			this.setState({
				revisions: ["current", ...response.json.map((s) => s.name)],
			});
		}
	};

	getSources = (rev) => {
		return LogsParts(rev).then((response) => {
			if (response.ok) {
				response.json.items[0] = "/";
				this.setState({ rev: rev, parts: response.json.items });
			}
		});
	};

	onChangeRev = (event) => {
		const rev = event.target.value;
		this.getSources(rev);
	};

	onChangeSource = (event) => {
		this.setState({
			source: Array.from(event.target.selectedOptions).map(
				(o) => o.value,
			),
		});
	};

	render() {
		if (this.state.status === LOGS_FILTER_STATUS.LOADING) {
			return <Loading />;
		}
		return (
			<div className="row">
				<h5>Filter logs by</h5>
				<div className="col-sm-2 mt-3">
					<label htmlFor="revision" className="form-label">
						Revision
					</label>
					<select
						className="form-select"
						name="revision"
						id="revision"
						defaultValue={this.state.rev}
						onChange={this.onChangeRev}
					>
						{this.state.revisions.map((rev) => (
							<option value={rev} key={rev}>
								{rev}
							</option>
						))}
					</select>
				</div>
				<div className="col-sm-10 mt-3">
					{this.state.parts.length > 0 && (
						<React.Fragment>
							<label htmlFor="part" className="form-label">
								Log Fragment
							</label>
							<select
								style={{ height: "200px" }}
								multiple={true}
								className="form-select"
								name="part"
								id="part"
								defaultValue={this.state.source}
								onChange={this.onChangeSource}
							>
								{this.state.parts.map((s) => (
									<option value={s} key={s}>
										{s}
									</option>
								))}
							</select>
						</React.Fragment>
					)}
				</div>
			</div>
		);
	}
}
