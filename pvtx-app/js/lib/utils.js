const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
const k = 1024;
const maxViewableFileSize = k * 100;

export const capitalize = (string, divider = " ") =>
	string
		.toLowerCase()
		.split(divider)
		.map((word) => word[0].toUpperCase() + word.slice(1, word.length))
		.join(" ");

export const autoPartial = (func) => {
	const numberOfArguments = func.length;
	const newFunc = (...args) => {
		return args.length < numberOfArguments
			? autoPartial(func.bind(null, ...args))
			: func(...args);
	};
	Object.defineProperty(newFunc, "length", {
		value: numberOfArguments,
		writable: false,
	});
	return newFunc;
};

export const PropsToQueryString = (props = {}) => {
	return Object.keys(props).reduce((query, key, index) => {
		const separator = index > 0 ? "/&" : "?";
		return `${query}${separator}${key}=${props[key]}`;
	}, "");
};

export const Slugify = (string) => {
	const a =
		"àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;";
	const b =
		"aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------";
	const p = new RegExp(a.split("").join("|"), "g");

	return string
		.toString()
		.toLowerCase()
		.replace(/\s+/g, "-") // Replace spaces with -
		.replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
		.replace(/&/g, "-and-") // Replace & with 'and'
		.replace(/[^\w-]+/g, "") // Remove all non-word characters
		.replace(/--+/g, "-") // Replace multiple - with single -
		.replace(/^-+/, "") // Trim - from start of text
		.replace(/-+$/, ""); // Trim - from end of text
};

export function resolvePath(obj, path, defaultReturn) {
	return path.split(".").reduce(function (prev, curr) {
		return prev && prev.hasOwnProperty(curr) ? prev[curr] : defaultReturn;
	}, obj);
}

export function flatObject(input) {
	return Object.keys(input).reduce((prev, curr) => {
		return flat(prev, curr, input[curr]);
	}, {});
}

function flat(res, key, val, pre = "") {
	const prefix = [pre, key].filter((v) => v).join(".");
	return typeof val === "object" && !!val
		? Object.keys(val).reduce(
				(prev, curr) => flat(prev, curr, val[curr], prefix),
				res,
			)
		: Object.assign(res, { [prefix]: val });
}

export const mergeObject = (a, b) => {
	const c = { ...a };
	for (const k in b) {
		if (b[k] && b[k] !== "") {
			c[k] = b[k];
		}
	}
	return c;
};

export function GetFileSize(bytes = 0, decimals = 2) {
	if (bytes === 0 || !bytes) {
		return new FSize();
	}

	return new FSize(bytes, decimals);
}

class FSize {
	constructor(bytes = 0, decimals = 2) {
		const dm = Math.max(decimals, 0);
		const i = Math.floor(Math.log(bytes) / Math.log(k));

		this.amount = parseFloat((bytes / Math.pow(k, i)).toFixed(dm));
		this.unit = sizes[i];
	}

	toString() {
		return `${this.amount} ${this.unit}`;
	}
}
