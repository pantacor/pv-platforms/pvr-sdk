import { describe, expect, test } from "@jest/globals";
import {
	CreateFsDescription,
	TYPES,
	Package,
	Signature,
	Packages,
} from "./state";

/**
 * @jest-environment jsdom
 */

describe("Create fylesystem description from state", () => {
	test("Create filesystem with signature without config", () => {
		const state = {
			"_config/awconnect/etc/wifi-connect/conf.d/WifiHotspot.json": {
				psk: "p4nt4c0r",
				ssid: "pantacor",
			},
			"_sigs/pvr-sdk.json": {
				"#spec": "pvs@2",
				protected:
					"eyJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJuIjoidjRJa0w1MzBYRlkyT1V4aG9vOWVRaWZ1TmxRRk9mOGxHblowUlVGSm9aTkRRNWlHRWlxeXhNMEtpOVh5UUpVV1g2RHpXY3lSVVZPUklJS29hRS1yZlB3YUh1QmdOVDRXY2RzRjN6NTRsWGQ5TUNsdWdfejl5bEgtVFNLUGFKam9ORlAyTVdDX0p5dXUxbG5ucW5iNUdzUlQ1WFh1RlA4MTNwVDlnX25fRmFWbDNuendMUnQ5ZmhHOGs3Y1M5MlJyR2Y0R1pqQnYzUEtORnJWOHBvTmZ1aDI5WHJtcTBWc3ktVGpPNUphT2Q2bzVFcXhGZXBKTnMtS21mMUdMZjlfYS1rN0I3YUQyQWdKYy1hSk9GZ2VEVjNtUFBESXFfbmtPeG9tYXA0a3ZlR0RjbkxMcDg2T1VkUEJaLWVjZHl4ZmhRT3plcm44c2o4ODQ1bmhmWlpPRkN3IiwiZSI6IkFRQUIifSwicHZzIjp7ImluY2x1ZGUiOlsicHZyLXNkay8qKiIsIl9jb25maWcvcHZyLXNkay8qKiJdLCJleGNsdWRlIjpbInB2ci1zZGsvc3JjLmpzb24iLCJfc2lncy9wdnItc2RrLmpzb24iXX0sInR5cCI6IlBWUyIsIng1YyI6WyJNSUlEYWpDQ0FsS2dBd0lCQWdJUkFLQnNoZS9Bd1A5ZGI2dU9TSUxTdThJd0RRWUpLb1pJaHZjTkFRRUxCUUF3SERFYU1CZ0dBMVVFQXd3UlVHRnVkR0YyYVhOdmNpQkVaWFlnUTBFd0hoY05Nakl3TmpFeU1UQXdNREU0V2hjTk1qUXdPVEUwTVRBd01ERTRXakFhTVJnd0ZnWURWUVFEREE5d2RpMWtaWFpsYkc5d1pYSXRNREV3Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRQy9naVF2bmZSY1ZqWTVUR0dpajE1Q0orNDJWQVU1L3lVYWRuUkZRVW1oazBORG1JWVNLckxFelFxTDFmSkFsUlpmb1BOWnpKRlJVNUVnZ3Fob1Q2dDgvQm9lNEdBMVBoWngyd1hmUG5pVmQzMHdLVzZEL1AzS1VmNU5JbzlvbU9nMFUvWXhZTDhuSzY3V1dlZXFkdmtheEZQbGRlNFUvelhlbFAyRCtmOFZwV1hlZlBBdEczMStFYnlUdHhMM1pHc1ovZ1ptTUcvYzhvMFd0WHltZzErNkhiMWV1YXJSV3pMNU9NN2tsbzUzcWprU3JFVjZrazJ6NHFaL1VZdC8zOXI2VHNIdG9QWUNBbHo1b2s0V0I0TlhlWTg4TWlyK2VRN0dpWnFuaVM5NFlOeWNzdW56bzVSMDhGbjU1eDNMRitGQTdONnVmeXlQenpqbWVGOWxrNFVMQWdNQkFBR2pnYWd3Z2FVd0NRWURWUjBUQkFJd0FEQWRCZ05WSFE0RUZnUVUxZTN4WmhIUkJKejJKSkp5UjE2ZjJMRzIyR013VndZRFZSMGpCRkF3VG9BVVdZdVErcEtDNElicG1DbXZFL2o0SWVxUmdUMmhJS1FlTUJ3eEdqQVlCZ05WQkFNTUVWQmhiblJoZG1semIzSWdSR1YySUVOQmdoUmpjUVowZWlxcXRCazgwZUJqSUFTVFNjV0p5akFUQmdOVkhTVUVEREFLQmdnckJnRUZCUWNEQXpBTEJnTlZIUThFQkFNQ0I0QXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBQlcwVUN6SDVtN2txNFE0Yi9qelFLM05QeTlQRjhwOWxoOEtzNWR2SElBUWJMbU1LeUZiWFFpcm5YcWVlZUhYOStNVEU2TDdtSlVaNkVab2JlMWppSjZURzNwcnE4M1dyUUhFaEJQV1k5NWFsZFBBblhCeEhPS3o5by9xYTMrZVVZWHVoTVorUFBiWWRjbVpvZTgyQTc3SlZzRzFKRmNlaURsWFZ6YjgvK2p1bWd4MFUzc1hXN2FBcjFaV2FNNFZqWkx0bGZ3Qk9KL1FDTThsdFJhUjJPejdtVGpmVTUzWlgzUThzVkFUR2VVRWhHbFN6SEhUQjQvb01nRnZ3YWhPaWZTeTJuUk1FL0F5L0NqTDMvSDIxRTBHRHAvaUJvQWdQbzcvQWhNZEU5aUYyV2d0Y0F3RVVFcHZqdldJNFlrN1libTMzUEl0VGhsN0NDTmNvNHVOTzlvPSJdfQ",
				signature:
					"NIviuNqrdvZPrwcqTisqW4nluYZ1yv9SapqMQINaBIY6YPeWw4OZ74PyB8z50rKsHprzNPOrMIJwYt51bEA0MJLpcJIaPZDy5pY_WURsjgEJ686l4s8aibDygm6YbiSJ4idJGXeA7TgsBPK0h0YXd0S-MAB0gbEOhFA_8UvAXV-R3brep8vrfdNAQaAHI_oQ0WkTxrJtjKXO5q7fL0Z9hjN7daHqYg9SyB1p_Gu8Vp5jATnshN2DqFr3S6VOGSSPvvnZldGniWbyROle4b_ijBNHaD67lQhDF8dPIzMX8lIhQ9lDT4BoFE-NlLMty27mylpZJHs7h4Ed1Y4DHVdV2w",
			},
			"pvr-sdk/_dm/root.squashfs.json": {
				data_device: "root.squashfs",
				hash_device: "root.squashfs.hash",
				root_hash:
					"8f704d7548af4e0fe854057fe22a60e0f344016809a80fcffeb81b5d6df86046",
				salt: "64440fb2cd2374345715ed8fa574e5b798d97b30b8d90d79d70012f4ac97462b",
				type: "dm-verity",
				uuid: "125e28bb-90af-4d13-b72b-c14549476299",
			},
			"pvr-sdk/lxc.container.conf":
				"a69205914de2e8b95270f94591d5c015796590da5273db35ef6b2ed40631fcca",
			"pvr-sdk/root.squashfs":
				"7d6dd737065bfae3cfd90526276ef7c016d1012ad10513429147bec0f3aa3463",
			"pvr-sdk/root.squashfs.docker-digest":
				"21c55831331afa7eccd762d8ae2292f459550d868f5d03a09bc1fe59f8584272",
			"pvr-sdk/root.squashfs.hash":
				"ee0aa80e68233030c759adad5c60cf5e461c726e35c34f93cc451d6ff2dd9b80",
			"pvr-sdk/run.json": {
				"#spec": "service-manifest-run@1",
				config: "lxc.container.conf",
				drivers: {
					manual: [],
					optional: [],
					required: [],
				},
				group: "platform",
				name: "pvr-sdk",
				restart_policy: "system",
				roles: ["mgmt"],
				"root-volume": "dm:root.squashfs",
				status_goal: "STARTED",
				storage: {
					"docker--etc-dropbear": {
						persistence: "permanent",
					},
					"docker--etc-volume": {
						persistence: "permanent",
					},
					"docker--home-pantavisor-.ssh": {
						persistence: "permanent",
					},
					"docker--var-pvr-sdk": {
						persistence: "permanent",
					},
					"lxc-overlay": {
						persistence: "boot",
					},
				},
				type: "lxc",
				volumes: [],
			},
			"pvr-sdk/src.json": {
				"#spec": "service-manifest-src@1",
				args: {
					PV_GROUP: "platform",
					PV_LXC_EXTRA_CONF:
						"lxc.mount.entry = /volumes/_pv/addons/plymouth/text-io var/run/plymouth-io-sockets none bind,rw,optional,create=dir 0 0",
					PV_RESTART_POLICY: "system",
					PV_ROLES: ["mgmt"],
					PV_SECURITY_WITH_STORAGE: "yes",
					PV_STATUS_GOAL: "STARTED",
				},
				dm_enabled: {
					"root.squashfs": true,
				},
				docker_config: {
					ArgsEscaped: true,
					Cmd: ["/sbin/init"],
					Env: [
						"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
						"PVR_DISABLE_SELF_UPGRADE=true",
						"PVR_CONFIG_DIR=/var/pvr-sdk/.pvr",
					],
					Volumes: {
						"/etc-volume": {},
						"/etc/dropbear": {},
						"/home/pantavisor/.ssh": {},
						"/var/pvr-sdk": {},
					},
					WorkingDir: "/workspace",
				},
				docker_digest:
					"registry.gitlab.com/pantacor/pv-platforms/pvr-sdk@sha256:66269a3f5050dbcb5d15ba9926d052026713bbd9bbc37963476ccb924bc606f5",
				docker_name:
					"registry.gitlab.com/pantacor/pv-platforms/pvr-sdk",
				docker_source: "remote,local",
				docker_tag: "arm32v6",
				template: "builtin-lxc-docker",
			},
		};

		const expected_parts: Packages = {
			_config: {
				id: "_config",
				name: "_config",
				type: TYPES.FOLDER,
				content: {
					"_config/awconnect": {
						id: "_config/awconnect",
						name: "awconnect",
						type: TYPES.FOLDER,
						content: {
							"_config/awconnect/etc": {
								id: "_config/awconnect/etc",
								name: "etc",
								type: TYPES.FOLDER,
								content: {
									"_config/awconnect/etc/wifi-connect": {
										id: "_config/awconnect/etc/wifi-connect",
										name: "wifi-connect",
										type: TYPES.FOLDER,
										content: {
											"_config/awconnect/etc/wifi-connect/conf.d":
												{
													id: "_config/awconnect/etc/wifi-connect/conf.d",
													name: "conf.d",
													type: TYPES.FOLDER,
													content: {
														"_config/awconnect/etc/wifi-connect/conf.d/WifiHotspot.json":
															{
																id: "_config/awconnect/etc/wifi-connect/conf.d/WifiHotspot.json",
																name: "WifiHotspot.json",
																type: TYPES.JSON,
																content: {
																	psk: "p4nt4c0r",
																	ssid: "pantacor",
																},
															},
													},
												},
										},
									},
								},
							},
						},
					},
				},
			},
			"pvr-sdk": {
				id: "_sigs/pvr-sdk.json",
				name: "pvr-sdk",
				type: TYPES.PACKAGE,
				included: new Set([
					"_sigs/pvr-sdk.json",
					"pvr-sdk/_dm/root.squashfs.json",
					"pvr-sdk/lxc.container.conf",
					"pvr-sdk/root.squashfs",
					"pvr-sdk/root.squashfs.docker-digest",
					"pvr-sdk/root.squashfs.hash",
					"pvr-sdk/run.json",
					"pvr-sdk/src.json",
				]),
				content: {
					[TYPES.SIGNATURE]: {
						id: "_sigs/pvr-sdk.json",
						name: "pvr-sdk.json",
						type: TYPES.SIGNATURE,
						content: {
							alg: "RS256",
							jwk: {
								kty: "RSA",
								n: "v4IkL530XFY2OUxhoo9eQifuNlQFOf8lGnZ0RUFJoZNDQ5iGEiqyxM0Ki9XyQJUWX6DzWcyRUVORIIKoaE-rfPwaHuBgNT4WcdsF3z54lXd9MClug_z9ylH-TSKPaJjoNFP2MWC_Jyuu1lnnqnb5GsRT5XXuFP813pT9g_n_FaVl3nzwLRt9fhG8k7cS92RrGf4GZjBv3PKNFrV8poNfuh29Xrmq0Vsy-TjO5JaOd6o5EqxFepJNs-Kmf1GLf9_a-k7B7aD2AgJc-aJOFgeDV3mPPDIq_nkOxomap4kveGDcnLLp86OUdPBZ-ecdyxfhQOzern8sj8845nhfZZOFCw",
								e: "AQAB",
							},
							pvs: {
								include: ["pvr-sdk/**", "_config/pvr-sdk/**"],
								exclude: [
									"pvr-sdk/src.json",
									"_sigs/pvr-sdk.json",
								],
							},
							typ: "PVS",
							x5c: [
								"MIIDajCCAlKgAwIBAgIRAKBshe/AwP9db6uOSILSu8IwDQYJKoZIhvcNAQELBQAwHDEaMBgGA1UEAwwRUGFudGF2aXNvciBEZXYgQ0EwHhcNMjIwNjEyMTAwMDE4WhcNMjQwOTE0MTAwMDE4WjAaMRgwFgYDVQQDDA9wdi1kZXZlbG9wZXItMDEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC/giQvnfRcVjY5TGGij15CJ+42VAU5/yUadnRFQUmhk0NDmIYSKrLEzQqL1fJAlRZfoPNZzJFRU5EggqhoT6t8/Boe4GA1PhZx2wXfPniVd30wKW6D/P3KUf5NIo9omOg0U/YxYL8nK67WWeeqdvkaxFPlde4U/zXelP2D+f8VpWXefPAtG31+EbyTtxL3ZGsZ/gZmMG/c8o0WtXymg1+6Hb1euarRWzL5OM7klo53qjkSrEV6kk2z4qZ/UYt/39r6TsHtoPYCAlz5ok4WB4NXeY88Mir+eQ7GiZqniS94YNycsunzo5R08Fn55x3LF+FA7N6ufyyPzzjmeF9lk4ULAgMBAAGjgagwgaUwCQYDVR0TBAIwADAdBgNVHQ4EFgQU1e3xZhHRBJz2JJJyR16f2LG22GMwVwYDVR0jBFAwToAUWYuQ+pKC4IbpmCmvE/j4IeqRgT2hIKQeMBwxGjAYBgNVBAMMEVBhbnRhdmlzb3IgRGV2IENBghRjcQZ0eiqqtBk80eBjIASTScWJyjATBgNVHSUEDDAKBggrBgEFBQcDAzALBgNVHQ8EBAMCB4AwDQYJKoZIhvcNAQELBQADggEBABW0UCzH5m7kq4Q4b/jzQK3NPy9PF8p9lh8Ks5dvHIAQbLmMKyFbXQirnXqeeeHX9+MTE6L7mJUZ6EZobe1jiJ6TG3prq83WrQHEhBPWY95aldPAnXBxHOKz9o/qa3+eUYXuhMZ+PPbYdcmZoe82A77JVsG1JFceiDlXVzb8/+jumgx0U3sXW7aAr1ZWaM4VjZLtlfwBOJ/QCM8ltRaR2Oz7mTjfU53ZX3Q8sVATGeUEhGlSzHHTB4/oMgFvwahOifSy2nRME/Ay/CjL3/H21E0GDp/iBoAgPo7/AhMdE9iF2WgtcAwEUEpvjvWI4Yk7Ybm33PItThl7CCNco4uNO9o=",
							],
						} as Signature,
						raw: {
							"#spec": "pvs@2",
							protected:
								"eyJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJuIjoidjRJa0w1MzBYRlkyT1V4aG9vOWVRaWZ1TmxRRk9mOGxHblowUlVGSm9aTkRRNWlHRWlxeXhNMEtpOVh5UUpVV1g2RHpXY3lSVVZPUklJS29hRS1yZlB3YUh1QmdOVDRXY2RzRjN6NTRsWGQ5TUNsdWdfejl5bEgtVFNLUGFKam9ORlAyTVdDX0p5dXUxbG5ucW5iNUdzUlQ1WFh1RlA4MTNwVDlnX25fRmFWbDNuendMUnQ5ZmhHOGs3Y1M5MlJyR2Y0R1pqQnYzUEtORnJWOHBvTmZ1aDI5WHJtcTBWc3ktVGpPNUphT2Q2bzVFcXhGZXBKTnMtS21mMUdMZjlfYS1rN0I3YUQyQWdKYy1hSk9GZ2VEVjNtUFBESXFfbmtPeG9tYXA0a3ZlR0RjbkxMcDg2T1VkUEJaLWVjZHl4ZmhRT3plcm44c2o4ODQ1bmhmWlpPRkN3IiwiZSI6IkFRQUIifSwicHZzIjp7ImluY2x1ZGUiOlsicHZyLXNkay8qKiIsIl9jb25maWcvcHZyLXNkay8qKiJdLCJleGNsdWRlIjpbInB2ci1zZGsvc3JjLmpzb24iLCJfc2lncy9wdnItc2RrLmpzb24iXX0sInR5cCI6IlBWUyIsIng1YyI6WyJNSUlEYWpDQ0FsS2dBd0lCQWdJUkFLQnNoZS9Bd1A5ZGI2dU9TSUxTdThJd0RRWUpLb1pJaHZjTkFRRUxCUUF3SERFYU1CZ0dBMVVFQXd3UlVHRnVkR0YyYVhOdmNpQkVaWFlnUTBFd0hoY05Nakl3TmpFeU1UQXdNREU0V2hjTk1qUXdPVEUwTVRBd01ERTRXakFhTVJnd0ZnWURWUVFEREE5d2RpMWtaWFpsYkc5d1pYSXRNREV3Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRQy9naVF2bmZSY1ZqWTVUR0dpajE1Q0orNDJWQVU1L3lVYWRuUkZRVW1oazBORG1JWVNLckxFelFxTDFmSkFsUlpmb1BOWnpKRlJVNUVnZ3Fob1Q2dDgvQm9lNEdBMVBoWngyd1hmUG5pVmQzMHdLVzZEL1AzS1VmNU5JbzlvbU9nMFUvWXhZTDhuSzY3V1dlZXFkdmtheEZQbGRlNFUvelhlbFAyRCtmOFZwV1hlZlBBdEczMStFYnlUdHhMM1pHc1ovZ1ptTUcvYzhvMFd0WHltZzErNkhiMWV1YXJSV3pMNU9NN2tsbzUzcWprU3JFVjZrazJ6NHFaL1VZdC8zOXI2VHNIdG9QWUNBbHo1b2s0V0I0TlhlWTg4TWlyK2VRN0dpWnFuaVM5NFlOeWNzdW56bzVSMDhGbjU1eDNMRitGQTdONnVmeXlQenpqbWVGOWxrNFVMQWdNQkFBR2pnYWd3Z2FVd0NRWURWUjBUQkFJd0FEQWRCZ05WSFE0RUZnUVUxZTN4WmhIUkJKejJKSkp5UjE2ZjJMRzIyR013VndZRFZSMGpCRkF3VG9BVVdZdVErcEtDNElicG1DbXZFL2o0SWVxUmdUMmhJS1FlTUJ3eEdqQVlCZ05WQkFNTUVWQmhiblJoZG1semIzSWdSR1YySUVOQmdoUmpjUVowZWlxcXRCazgwZUJqSUFTVFNjV0p5akFUQmdOVkhTVUVEREFLQmdnckJnRUZCUWNEQXpBTEJnTlZIUThFQkFNQ0I0QXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBQlcwVUN6SDVtN2txNFE0Yi9qelFLM05QeTlQRjhwOWxoOEtzNWR2SElBUWJMbU1LeUZiWFFpcm5YcWVlZUhYOStNVEU2TDdtSlVaNkVab2JlMWppSjZURzNwcnE4M1dyUUhFaEJQV1k5NWFsZFBBblhCeEhPS3o5by9xYTMrZVVZWHVoTVorUFBiWWRjbVpvZTgyQTc3SlZzRzFKRmNlaURsWFZ6YjgvK2p1bWd4MFUzc1hXN2FBcjFaV2FNNFZqWkx0bGZ3Qk9KL1FDTThsdFJhUjJPejdtVGpmVTUzWlgzUThzVkFUR2VVRWhHbFN6SEhUQjQvb01nRnZ3YWhPaWZTeTJuUk1FL0F5L0NqTDMvSDIxRTBHRHAvaUJvQWdQbzcvQWhNZEU5aUYyV2d0Y0F3RVVFcHZqdldJNFlrN1libTMzUEl0VGhsN0NDTmNvNHVOTzlvPSJdfQ",
							signature:
								"NIviuNqrdvZPrwcqTisqW4nluYZ1yv9SapqMQINaBIY6YPeWw4OZ74PyB8z50rKsHprzNPOrMIJwYt51bEA0MJLpcJIaPZDy5pY_WURsjgEJ686l4s8aibDygm6YbiSJ4idJGXeA7TgsBPK0h0YXd0S-MAB0gbEOhFA_8UvAXV-R3brep8vrfdNAQaAHI_oQ0WkTxrJtjKXO5q7fL0Z9hjN7daHqYg9SyB1p_Gu8Vp5jATnshN2DqFr3S6VOGSSPvvnZldGniWbyROle4b_ijBNHaD67lQhDF8dPIzMX8lIhQ9lDT4BoFE-NlLMty27mylpZJHs7h4Ed1Y4DHVdV2w",
						},
					},
					"pvr-sdk": {
						id: "pvr-sdk",
						name: "pvr-sdk",
						type: TYPES.FOLDER,
						content: {
							"pvr-sdk/_dm": {
								id: "pvr-sdk/_dm",
								name: "_dm",
								type: TYPES.FOLDER,
								content: {
									"pvr-sdk/_dm/root.squashfs.json": {
										id: "pvr-sdk/_dm/root.squashfs.json",
										name: "root.squashfs.json",
										type: TYPES.JSON,
										content: {
											data_device: "root.squashfs",
											hash_device: "root.squashfs.hash",
											root_hash:
												"8f704d7548af4e0fe854057fe22a60e0f344016809a80fcffeb81b5d6df86046",
											salt: "64440fb2cd2374345715ed8fa574e5b798d97b30b8d90d79d70012f4ac97462b",
											type: "dm-verity",
											uuid: "125e28bb-90af-4d13-b72b-c14549476299",
										},
									},
								},
							},
							"pvr-sdk/lxc.container.conf": {
								id: "pvr-sdk/lxc.container.conf",
								name: "lxc.container.conf",
								type: TYPES.OBJECT,
								content:
									"a69205914de2e8b95270f94591d5c015796590da5273db35ef6b2ed40631fcca",
							},
							"pvr-sdk/root.squashfs": {
								id: "pvr-sdk/root.squashfs",
								name: "root.squashfs",
								type: TYPES.OBJECT,
								content:
									"7d6dd737065bfae3cfd90526276ef7c016d1012ad10513429147bec0f3aa3463",
							},
							"pvr-sdk/root.squashfs.docker-digest": {
								id: "pvr-sdk/root.squashfs.docker-digest",
								name: "root.squashfs.docker-digest",
								type: TYPES.OBJECT,
								content:
									"21c55831331afa7eccd762d8ae2292f459550d868f5d03a09bc1fe59f8584272",
							},
							"pvr-sdk/root.squashfs.hash": {
								id: "pvr-sdk/root.squashfs.hash",
								name: "root.squashfs.hash",
								type: TYPES.OBJECT,
								content:
									"ee0aa80e68233030c759adad5c60cf5e461c726e35c34f93cc451d6ff2dd9b80",
							},
							"pvr-sdk/run.json": {
								id: "pvr-sdk/run.json",
								name: "run.json",
								type: TYPES.JSON,
								content: {
									"#spec": "service-manifest-run@1",
									config: "lxc.container.conf",
									drivers: {
										manual: [],
										optional: [],
										required: [],
									},
									group: "platform",
									name: "pvr-sdk",
									restart_policy: "system",
									roles: ["mgmt"],
									"root-volume": "dm:root.squashfs",
									status_goal: "STARTED",
									storage: {
										"docker--etc-dropbear": {
											persistence: "permanent",
										},
										"docker--etc-volume": {
											persistence: "permanent",
										},
										"docker--home-pantavisor-.ssh": {
											persistence: "permanent",
										},
										"docker--var-pvr-sdk": {
											persistence: "permanent",
										},
										"lxc-overlay": {
											persistence: "boot",
										},
									},
									type: "lxc",
									volumes: [],
								},
							},
							"pvr-sdk/src.json": {
								id: "pvr-sdk/src.json",
								name: "src.json",
								type: TYPES.JSON,
								content: {
									"#spec": "service-manifest-src@1",
									args: {
										PV_GROUP: "platform",
										PV_LXC_EXTRA_CONF:
											"lxc.mount.entry = /volumes/_pv/addons/plymouth/text-io var/run/plymouth-io-sockets none bind,rw,optional,create=dir 0 0",
										PV_RESTART_POLICY: "system",
										PV_ROLES: ["mgmt"],
										PV_SECURITY_WITH_STORAGE: "yes",
										PV_STATUS_GOAL: "STARTED",
									},
									dm_enabled: {
										"root.squashfs": true,
									},
									docker_config: {
										ArgsEscaped: true,
										Cmd: ["/sbin/init"],
										Env: [
											"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
											"PVR_DISABLE_SELF_UPGRADE=true",
											"PVR_CONFIG_DIR=/var/pvr-sdk/.pvr",
										],
										Volumes: {
											"/etc-volume": {},
											"/etc/dropbear": {},
											"/home/pantavisor/.ssh": {},
											"/var/pvr-sdk": {},
										},
										WorkingDir: "/workspace",
									},
									docker_digest:
										"registry.gitlab.com/pantacor/pv-platforms/pvr-sdk@sha256:66269a3f5050dbcb5d15ba9926d052026713bbd9bbc37963476ccb924bc606f5",
									docker_name:
										"registry.gitlab.com/pantacor/pv-platforms/pvr-sdk",
									docker_source: "remote,local",
									docker_tag: "arm32v6",
									template: "builtin-lxc-docker",
								},
							},
						},
					},
				},
			},
		};

		const parts = CreateFsDescription(state);
		expect(expected_parts).toStrictEqual(parts);
	});

	test("Create filesystem with signature with config", () => {
		const state = {
			"_config/awconnect/etc/wifi-connect/conf.d/WifiHotspot.json": {
				psk: "p4nt4c0r",
				ssid: "pantacor",
			},
			"_config/pvr-sdk/etc/pvr-sdk/config.json": {
				httpd: {
					listen: "0.0.0.0",
					port: "12368",
				},
			},
			"_sigs/pvr-sdk.json": {
				"#spec": "pvs@2",
				protected:
					"eyJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJuIjoidjRJa0w1MzBYRlkyT1V4aG9vOWVRaWZ1TmxRRk9mOGxHblowUlVGSm9aTkRRNWlHRWlxeXhNMEtpOVh5UUpVV1g2RHpXY3lSVVZPUklJS29hRS1yZlB3YUh1QmdOVDRXY2RzRjN6NTRsWGQ5TUNsdWdfejl5bEgtVFNLUGFKam9ORlAyTVdDX0p5dXUxbG5ucW5iNUdzUlQ1WFh1RlA4MTNwVDlnX25fRmFWbDNuendMUnQ5ZmhHOGs3Y1M5MlJyR2Y0R1pqQnYzUEtORnJWOHBvTmZ1aDI5WHJtcTBWc3ktVGpPNUphT2Q2bzVFcXhGZXBKTnMtS21mMUdMZjlfYS1rN0I3YUQyQWdKYy1hSk9GZ2VEVjNtUFBESXFfbmtPeG9tYXA0a3ZlR0RjbkxMcDg2T1VkUEJaLWVjZHl4ZmhRT3plcm44c2o4ODQ1bmhmWlpPRkN3IiwiZSI6IkFRQUIifSwicHZzIjp7ImluY2x1ZGUiOlsicHZyLXNkay8qKiIsIl9jb25maWcvcHZyLXNkay8qKiJdLCJleGNsdWRlIjpbInB2ci1zZGsvc3JjLmpzb24iLCJfc2lncy9wdnItc2RrLmpzb24iXX0sInR5cCI6IlBWUyIsIng1YyI6WyJNSUlEYWpDQ0FsS2dBd0lCQWdJUkFLQnNoZS9Bd1A5ZGI2dU9TSUxTdThJd0RRWUpLb1pJaHZjTkFRRUxCUUF3SERFYU1CZ0dBMVVFQXd3UlVHRnVkR0YyYVhOdmNpQkVaWFlnUTBFd0hoY05Nakl3TmpFeU1UQXdNREU0V2hjTk1qUXdPVEUwTVRBd01ERTRXakFhTVJnd0ZnWURWUVFEREE5d2RpMWtaWFpsYkc5d1pYSXRNREV3Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRQy9naVF2bmZSY1ZqWTVUR0dpajE1Q0orNDJWQVU1L3lVYWRuUkZRVW1oazBORG1JWVNLckxFelFxTDFmSkFsUlpmb1BOWnpKRlJVNUVnZ3Fob1Q2dDgvQm9lNEdBMVBoWngyd1hmUG5pVmQzMHdLVzZEL1AzS1VmNU5JbzlvbU9nMFUvWXhZTDhuSzY3V1dlZXFkdmtheEZQbGRlNFUvelhlbFAyRCtmOFZwV1hlZlBBdEczMStFYnlUdHhMM1pHc1ovZ1ptTUcvYzhvMFd0WHltZzErNkhiMWV1YXJSV3pMNU9NN2tsbzUzcWprU3JFVjZrazJ6NHFaL1VZdC8zOXI2VHNIdG9QWUNBbHo1b2s0V0I0TlhlWTg4TWlyK2VRN0dpWnFuaVM5NFlOeWNzdW56bzVSMDhGbjU1eDNMRitGQTdONnVmeXlQenpqbWVGOWxrNFVMQWdNQkFBR2pnYWd3Z2FVd0NRWURWUjBUQkFJd0FEQWRCZ05WSFE0RUZnUVUxZTN4WmhIUkJKejJKSkp5UjE2ZjJMRzIyR013VndZRFZSMGpCRkF3VG9BVVdZdVErcEtDNElicG1DbXZFL2o0SWVxUmdUMmhJS1FlTUJ3eEdqQVlCZ05WQkFNTUVWQmhiblJoZG1semIzSWdSR1YySUVOQmdoUmpjUVowZWlxcXRCazgwZUJqSUFTVFNjV0p5akFUQmdOVkhTVUVEREFLQmdnckJnRUZCUWNEQXpBTEJnTlZIUThFQkFNQ0I0QXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBQlcwVUN6SDVtN2txNFE0Yi9qelFLM05QeTlQRjhwOWxoOEtzNWR2SElBUWJMbU1LeUZiWFFpcm5YcWVlZUhYOStNVEU2TDdtSlVaNkVab2JlMWppSjZURzNwcnE4M1dyUUhFaEJQV1k5NWFsZFBBblhCeEhPS3o5by9xYTMrZVVZWHVoTVorUFBiWWRjbVpvZTgyQTc3SlZzRzFKRmNlaURsWFZ6YjgvK2p1bWd4MFUzc1hXN2FBcjFaV2FNNFZqWkx0bGZ3Qk9KL1FDTThsdFJhUjJPejdtVGpmVTUzWlgzUThzVkFUR2VVRWhHbFN6SEhUQjQvb01nRnZ3YWhPaWZTeTJuUk1FL0F5L0NqTDMvSDIxRTBHRHAvaUJvQWdQbzcvQWhNZEU5aUYyV2d0Y0F3RVVFcHZqdldJNFlrN1libTMzUEl0VGhsN0NDTmNvNHVOTzlvPSJdfQ",
				signature:
					"NIviuNqrdvZPrwcqTisqW4nluYZ1yv9SapqMQINaBIY6YPeWw4OZ74PyB8z50rKsHprzNPOrMIJwYt51bEA0MJLpcJIaPZDy5pY_WURsjgEJ686l4s8aibDygm6YbiSJ4idJGXeA7TgsBPK0h0YXd0S-MAB0gbEOhFA_8UvAXV-R3brep8vrfdNAQaAHI_oQ0WkTxrJtjKXO5q7fL0Z9hjN7daHqYg9SyB1p_Gu8Vp5jATnshN2DqFr3S6VOGSSPvvnZldGniWbyROle4b_ijBNHaD67lQhDF8dPIzMX8lIhQ9lDT4BoFE-NlLMty27mylpZJHs7h4Ed1Y4DHVdV2w",
			},
			"_sigs/_hostconfig.json": {
				"#spec": "pvs@2",
				protected:
					"eyJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJuIjoidjRJa0w1MzBYRlkyT1V4aG9vOWVRaWZ1TmxRRk9mOGxHblowUlVGSm9aTkRRNWlHRWlxeXhNMEtpOVh5UUpVV1g2RHpXY3lSVVZPUklJS29hRS1yZlB3YUh1QmdOVDRXY2RzRjN6NTRsWGQ5TUNsdWdfejl5bEgtVFNLUGFKam9ORlAyTVdDX0p5dXUxbG5ucW5iNUdzUlQ1WFh1RlA4MTNwVDlnX25fRmFWbDNuendMUnQ5ZmhHOGs3Y1M5MlJyR2Y0R1pqQnYzUEtORnJWOHBvTmZ1aDI5WHJtcTBWc3ktVGpPNUphT2Q2bzVFcXhGZXBKTnMtS21mMUdMZjlfYS1rN0I3YUQyQWdKYy1hSk9GZ2VEVjNtUFBESXFfbmtPeG9tYXA0a3ZlR0RjbkxMcDg2T1VkUEJaLWVjZHl4ZmhRT3plcm44c2o4ODQ1bmhmWlpPRkN3IiwiZSI6IkFRQUIifSwicHZzIjp7ImluY2x1ZGUiOlsiX2hvc3Rjb25maWcvKioiXSwiZXhjbHVkZSI6WyJzcmMuanNvbiIsIl9zaWdzLy5qc29uIl19LCJ0eXAiOiJQVlMiLCJ4NWMiOlsiTUlJRGFqQ0NBbEtnQXdJQkFnSVJBS0JzaGUvQXdQOWRiNnVPU0lMU3U4SXdEUVlKS29aSWh2Y05BUUVMQlFBd0hERWFNQmdHQTFVRUF3d1JVR0Z1ZEdGMmFYTnZjaUJFWlhZZ1EwRXdIaGNOTWpJd05qRXlNVEF3TURFNFdoY05NalF3T1RFME1UQXdNREU0V2pBYU1SZ3dGZ1lEVlFRRERBOXdkaTFrWlhabGJHOXdaWEl0TURFd2dnRWlNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0SUJEd0F3Z2dFS0FvSUJBUUMvZ2lRdm5mUmNWalk1VEdHaWoxNUNKKzQyVkFVNS95VWFkblJGUVVtaGswTkRtSVlTS3JMRXpRcUwxZkpBbFJaZm9QTlp6SkZSVTVFZ2dxaG9UNnQ4L0JvZTRHQTFQaFp4MndYZlBuaVZkMzB3S1c2RC9QM0tVZjVOSW85b21PZzBVL1l4WUw4bks2N1dXZWVxZHZrYXhGUGxkZTRVL3pYZWxQMkQrZjhWcFdYZWZQQXRHMzErRWJ5VHR4TDNaR3NaL2dabU1HL2M4bzBXdFh5bWcxKzZIYjFldWFyUld6TDVPTTdrbG81M3Fqa1NyRVY2a2syejRxWi9VWXQvMzlyNlRzSHRvUFlDQWx6NW9rNFdCNE5YZVk4OE1pcitlUTdHaVpxbmlTOTRZTnljc3Vuem81UjA4Rm41NXgzTEYrRkE3TjZ1Znl5UHp6am1lRjlsazRVTEFnTUJBQUdqZ2Fnd2dhVXdDUVlEVlIwVEJBSXdBREFkQmdOVkhRNEVGZ1FVMWUzeFpoSFJCSnoySkpKeVIxNmYyTEcyMkdNd1Z3WURWUjBqQkZBd1RvQVVXWXVRK3BLQzRJYnBtQ212RS9qNEllcVJnVDJoSUtRZU1Cd3hHakFZQmdOVkJBTU1FVkJoYm5SaGRtbHpiM0lnUkdWMklFTkJnaFJqY1FaMGVpcXF0Qms4MGVCaklBU1RTY1dKeWpBVEJnTlZIU1VFRERBS0JnZ3JCZ0VGQlFjREF6QUxCZ05WSFE4RUJBTUNCNEF3RFFZSktvWklodmNOQVFFTEJRQURnZ0VCQUJXMFVDekg1bTdrcTRRNGIvanpRSzNOUHk5UEY4cDlsaDhLczVkdkhJQVFiTG1NS3lGYlhRaXJuWHFlZWVIWDkrTVRFNkw3bUpVWjZFWm9iZTFqaUo2VEczcHJxODNXclFIRWhCUFdZOTVhbGRQQW5YQnhIT0t6OW8vcWEzK2VVWVh1aE1aK1BQYllkY21ab2U4MkE3N0pWc0cxSkZjZWlEbFhWemI4LytqdW1neDBVM3NYVzdhQXIxWldhTTRWalpMdGxmd0JPSi9RQ004bHRSYVIyT3o3bVRqZlU1M1pYM1E4c1ZBVEdlVUVoR2xTekhIVEI0L29NZ0Z2d2FoT2lmU3kyblJNRS9BeS9DakwzL0gyMUUwR0RwL2lCb0FnUG83L0FoTWRFOWlGMldndGNBd0VVRXB2anZXSTRZazdZYm0zM1BJdFRobDdDQ05jbzR1Tk85bz0iXX0",
				signature:
					"QnjVDCHAZOnEF92MBEQdUDyP0ONrUnTIXbRCX6b3tEjSQZY_6ToHEoYnYxL4zv8VVZmeVw-AdMdBezP-V1cQF5_R3MbmBcECUpuQ7OpGZR8ITakX-K1XgIwpKt8g3Lfxdupe_fkVCCUZNFnT5arrL2LQq-L0cX0wO_y4xJTuymYTT1FCA-lb3pPpfJEpMvPuwTs2RGb_q4QuG3ZFKBAdzAZ7z58y0Uy8GtS-fJqcv0pPOiLGksdxDjhJYFttODlqpGVXpyv95M0aiip7KhyER8PORNwkncjvHpnmVHC_TGta9begZFilvNqZm0DSjwlCl9bl2h_9FQaRXycDkJbVqQ",
			},
			"_hostconfig/pvr/docker.json": {
				platforms: ["linux/arm64", "linux/arm"],
			},
			"pvr-sdk/_dm/root.squashfs.json": {
				data_device: "root.squashfs",
				hash_device: "root.squashfs.hash",
				root_hash:
					"8f704d7548af4e0fe854057fe22a60e0f344016809a80fcffeb81b5d6df86046",
				salt: "64440fb2cd2374345715ed8fa574e5b798d97b30b8d90d79d70012f4ac97462b",
				type: "dm-verity",
				uuid: "125e28bb-90af-4d13-b72b-c14549476299",
			},
			"pvr-sdk/lxc.container.conf":
				"a69205914de2e8b95270f94591d5c015796590da5273db35ef6b2ed40631fcca",
			"pvr-sdk/root.squashfs":
				"7d6dd737065bfae3cfd90526276ef7c016d1012ad10513429147bec0f3aa3463",
			"pvr-sdk/root.squashfs.docker-digest":
				"21c55831331afa7eccd762d8ae2292f459550d868f5d03a09bc1fe59f8584272",
			"pvr-sdk/root.squashfs.hash":
				"ee0aa80e68233030c759adad5c60cf5e461c726e35c34f93cc451d6ff2dd9b80",
			"pvr-sdk/run.json": {
				"#spec": "service-manifest-run@1",
				config: "lxc.container.conf",
				drivers: {
					manual: [],
					optional: [],
					required: [],
				},
				group: "platform",
				name: "pvr-sdk",
				restart_policy: "system",
				roles: ["mgmt"],
				"root-volume": "dm:root.squashfs",
				status_goal: "STARTED",
				storage: {
					"docker--etc-dropbear": {
						persistence: "permanent",
					},
					"docker--etc-volume": {
						persistence: "permanent",
					},
					"docker--home-pantavisor-.ssh": {
						persistence: "permanent",
					},
					"docker--var-pvr-sdk": {
						persistence: "permanent",
					},
					"lxc-overlay": {
						persistence: "boot",
					},
				},
				type: "lxc",
				volumes: [],
			},
			"pvr-sdk/src.json": {
				"#spec": "service-manifest-src@1",
				args: {
					PV_GROUP: "platform",
					PV_LXC_EXTRA_CONF:
						"lxc.mount.entry = /volumes/_pv/addons/plymouth/text-io var/run/plymouth-io-sockets none bind,rw,optional,create=dir 0 0",
					PV_RESTART_POLICY: "system",
					PV_ROLES: ["mgmt"],
					PV_SECURITY_WITH_STORAGE: "yes",
					PV_STATUS_GOAL: "STARTED",
				},
				dm_enabled: {
					"root.squashfs": true,
				},
				docker_config: {
					ArgsEscaped: true,
					Cmd: ["/sbin/init"],
					Env: [
						"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
						"PVR_DISABLE_SELF_UPGRADE=true",
						"PVR_CONFIG_DIR=/var/pvr-sdk/.pvr",
					],
					Volumes: {
						"/etc-volume": {},
						"/etc/dropbear": {},
						"/home/pantavisor/.ssh": {},
						"/var/pvr-sdk": {},
					},
					WorkingDir: "/workspace",
				},
				docker_digest:
					"registry.gitlab.com/pantacor/pv-platforms/pvr-sdk@sha256:66269a3f5050dbcb5d15ba9926d052026713bbd9bbc37963476ccb924bc606f5",
				docker_name:
					"registry.gitlab.com/pantacor/pv-platforms/pvr-sdk",
				docker_source: "remote,local",
				docker_tag: "arm32v6",
				template: "builtin-lxc-docker",
			},
		};

		const expected_parts: Packages = {
			_config: {
				id: "_config",
				name: "_config",
				type: TYPES.FOLDER,
				content: {
					"_config/awconnect": {
						id: "_config/awconnect",
						name: "awconnect",
						type: TYPES.FOLDER,
						content: {
							"_config/awconnect/etc": {
								id: "_config/awconnect/etc",
								name: "etc",
								type: TYPES.FOLDER,
								content: {
									"_config/awconnect/etc/wifi-connect": {
										id: "_config/awconnect/etc/wifi-connect",
										name: "wifi-connect",
										type: TYPES.FOLDER,
										content: {
											"_config/awconnect/etc/wifi-connect/conf.d":
												{
													id: "_config/awconnect/etc/wifi-connect/conf.d",
													name: "conf.d",
													type: TYPES.FOLDER,
													content: {
														"_config/awconnect/etc/wifi-connect/conf.d/WifiHotspot.json":
															{
																id: "_config/awconnect/etc/wifi-connect/conf.d/WifiHotspot.json",
																name: "WifiHotspot.json",
																type: TYPES.JSON,
																content: {
																	psk: "p4nt4c0r",
																	ssid: "pantacor",
																},
															},
													},
												},
										},
									},
								},
							},
						},
					},
				},
			},
			_hostconfig: {
				id: "_sigs/_hostconfig.json",
				name: "_hostconfig",
				type: TYPES.PACKAGE,
				included: new Set([
					"_sigs/_hostconfig.json",
					"_hostconfig/pvr/docker.json",
				]),
				content: {
					[TYPES.SIGNATURE]: {
						type: TYPES.SIGNATURE,
						id: "_sigs/_hostconfig.json",
						name: "_hostconfig.json",
						content: {
							alg: "RS256",
							jwk: {
								kty: "RSA",
								n: "v4IkL530XFY2OUxhoo9eQifuNlQFOf8lGnZ0RUFJoZNDQ5iGEiqyxM0Ki9XyQJUWX6DzWcyRUVORIIKoaE-rfPwaHuBgNT4WcdsF3z54lXd9MClug_z9ylH-TSKPaJjoNFP2MWC_Jyuu1lnnqnb5GsRT5XXuFP813pT9g_n_FaVl3nzwLRt9fhG8k7cS92RrGf4GZjBv3PKNFrV8poNfuh29Xrmq0Vsy-TjO5JaOd6o5EqxFepJNs-Kmf1GLf9_a-k7B7aD2AgJc-aJOFgeDV3mPPDIq_nkOxomap4kveGDcnLLp86OUdPBZ-ecdyxfhQOzern8sj8845nhfZZOFCw",
								e: "AQAB",
							},
							pvs: {
								include: ["_hostconfig/**"],
								exclude: ["src.json", "_sigs/.json"],
							},
							typ: "PVS",
							x5c: [
								"MIIDajCCAlKgAwIBAgIRAKBshe/AwP9db6uOSILSu8IwDQYJKoZIhvcNAQELBQAwHDEaMBgGA1UEAwwRUGFudGF2aXNvciBEZXYgQ0EwHhcNMjIwNjEyMTAwMDE4WhcNMjQwOTE0MTAwMDE4WjAaMRgwFgYDVQQDDA9wdi1kZXZlbG9wZXItMDEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC/giQvnfRcVjY5TGGij15CJ+42VAU5/yUadnRFQUmhk0NDmIYSKrLEzQqL1fJAlRZfoPNZzJFRU5EggqhoT6t8/Boe4GA1PhZx2wXfPniVd30wKW6D/P3KUf5NIo9omOg0U/YxYL8nK67WWeeqdvkaxFPlde4U/zXelP2D+f8VpWXefPAtG31+EbyTtxL3ZGsZ/gZmMG/c8o0WtXymg1+6Hb1euarRWzL5OM7klo53qjkSrEV6kk2z4qZ/UYt/39r6TsHtoPYCAlz5ok4WB4NXeY88Mir+eQ7GiZqniS94YNycsunzo5R08Fn55x3LF+FA7N6ufyyPzzjmeF9lk4ULAgMBAAGjgagwgaUwCQYDVR0TBAIwADAdBgNVHQ4EFgQU1e3xZhHRBJz2JJJyR16f2LG22GMwVwYDVR0jBFAwToAUWYuQ+pKC4IbpmCmvE/j4IeqRgT2hIKQeMBwxGjAYBgNVBAMMEVBhbnRhdmlzb3IgRGV2IENBghRjcQZ0eiqqtBk80eBjIASTScWJyjATBgNVHSUEDDAKBggrBgEFBQcDAzALBgNVHQ8EBAMCB4AwDQYJKoZIhvcNAQELBQADggEBABW0UCzH5m7kq4Q4b/jzQK3NPy9PF8p9lh8Ks5dvHIAQbLmMKyFbXQirnXqeeeHX9+MTE6L7mJUZ6EZobe1jiJ6TG3prq83WrQHEhBPWY95aldPAnXBxHOKz9o/qa3+eUYXuhMZ+PPbYdcmZoe82A77JVsG1JFceiDlXVzb8/+jumgx0U3sXW7aAr1ZWaM4VjZLtlfwBOJ/QCM8ltRaR2Oz7mTjfU53ZX3Q8sVATGeUEhGlSzHHTB4/oMgFvwahOifSy2nRME/Ay/CjL3/H21E0GDp/iBoAgPo7/AhMdE9iF2WgtcAwEUEpvjvWI4Yk7Ybm33PItThl7CCNco4uNO9o=",
							],
						},
						raw: {
							"#spec": "pvs@2",
							protected:
								"eyJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJuIjoidjRJa0w1MzBYRlkyT1V4aG9vOWVRaWZ1TmxRRk9mOGxHblowUlVGSm9aTkRRNWlHRWlxeXhNMEtpOVh5UUpVV1g2RHpXY3lSVVZPUklJS29hRS1yZlB3YUh1QmdOVDRXY2RzRjN6NTRsWGQ5TUNsdWdfejl5bEgtVFNLUGFKam9ORlAyTVdDX0p5dXUxbG5ucW5iNUdzUlQ1WFh1RlA4MTNwVDlnX25fRmFWbDNuendMUnQ5ZmhHOGs3Y1M5MlJyR2Y0R1pqQnYzUEtORnJWOHBvTmZ1aDI5WHJtcTBWc3ktVGpPNUphT2Q2bzVFcXhGZXBKTnMtS21mMUdMZjlfYS1rN0I3YUQyQWdKYy1hSk9GZ2VEVjNtUFBESXFfbmtPeG9tYXA0a3ZlR0RjbkxMcDg2T1VkUEJaLWVjZHl4ZmhRT3plcm44c2o4ODQ1bmhmWlpPRkN3IiwiZSI6IkFRQUIifSwicHZzIjp7ImluY2x1ZGUiOlsiX2hvc3Rjb25maWcvKioiXSwiZXhjbHVkZSI6WyJzcmMuanNvbiIsIl9zaWdzLy5qc29uIl19LCJ0eXAiOiJQVlMiLCJ4NWMiOlsiTUlJRGFqQ0NBbEtnQXdJQkFnSVJBS0JzaGUvQXdQOWRiNnVPU0lMU3U4SXdEUVlKS29aSWh2Y05BUUVMQlFBd0hERWFNQmdHQTFVRUF3d1JVR0Z1ZEdGMmFYTnZjaUJFWlhZZ1EwRXdIaGNOTWpJd05qRXlNVEF3TURFNFdoY05NalF3T1RFME1UQXdNREU0V2pBYU1SZ3dGZ1lEVlFRRERBOXdkaTFrWlhabGJHOXdaWEl0TURFd2dnRWlNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0SUJEd0F3Z2dFS0FvSUJBUUMvZ2lRdm5mUmNWalk1VEdHaWoxNUNKKzQyVkFVNS95VWFkblJGUVVtaGswTkRtSVlTS3JMRXpRcUwxZkpBbFJaZm9QTlp6SkZSVTVFZ2dxaG9UNnQ4L0JvZTRHQTFQaFp4MndYZlBuaVZkMzB3S1c2RC9QM0tVZjVOSW85b21PZzBVL1l4WUw4bks2N1dXZWVxZHZrYXhGUGxkZTRVL3pYZWxQMkQrZjhWcFdYZWZQQXRHMzErRWJ5VHR4TDNaR3NaL2dabU1HL2M4bzBXdFh5bWcxKzZIYjFldWFyUld6TDVPTTdrbG81M3Fqa1NyRVY2a2syejRxWi9VWXQvMzlyNlRzSHRvUFlDQWx6NW9rNFdCNE5YZVk4OE1pcitlUTdHaVpxbmlTOTRZTnljc3Vuem81UjA4Rm41NXgzTEYrRkE3TjZ1Znl5UHp6am1lRjlsazRVTEFnTUJBQUdqZ2Fnd2dhVXdDUVlEVlIwVEJBSXdBREFkQmdOVkhRNEVGZ1FVMWUzeFpoSFJCSnoySkpKeVIxNmYyTEcyMkdNd1Z3WURWUjBqQkZBd1RvQVVXWXVRK3BLQzRJYnBtQ212RS9qNEllcVJnVDJoSUtRZU1Cd3hHakFZQmdOVkJBTU1FVkJoYm5SaGRtbHpiM0lnUkdWMklFTkJnaFJqY1FaMGVpcXF0Qms4MGVCaklBU1RTY1dKeWpBVEJnTlZIU1VFRERBS0JnZ3JCZ0VGQlFjREF6QUxCZ05WSFE4RUJBTUNCNEF3RFFZSktvWklodmNOQVFFTEJRQURnZ0VCQUJXMFVDekg1bTdrcTRRNGIvanpRSzNOUHk5UEY4cDlsaDhLczVkdkhJQVFiTG1NS3lGYlhRaXJuWHFlZWVIWDkrTVRFNkw3bUpVWjZFWm9iZTFqaUo2VEczcHJxODNXclFIRWhCUFdZOTVhbGRQQW5YQnhIT0t6OW8vcWEzK2VVWVh1aE1aK1BQYllkY21ab2U4MkE3N0pWc0cxSkZjZWlEbFhWemI4LytqdW1neDBVM3NYVzdhQXIxWldhTTRWalpMdGxmd0JPSi9RQ004bHRSYVIyT3o3bVRqZlU1M1pYM1E4c1ZBVEdlVUVoR2xTekhIVEI0L29NZ0Z2d2FoT2lmU3kyblJNRS9BeS9DakwzL0gyMUUwR0RwL2lCb0FnUG83L0FoTWRFOWlGMldndGNBd0VVRXB2anZXSTRZazdZYm0zM1BJdFRobDdDQ05jbzR1Tk85bz0iXX0",
							signature:
								"QnjVDCHAZOnEF92MBEQdUDyP0ONrUnTIXbRCX6b3tEjSQZY_6ToHEoYnYxL4zv8VVZmeVw-AdMdBezP-V1cQF5_R3MbmBcECUpuQ7OpGZR8ITakX-K1XgIwpKt8g3Lfxdupe_fkVCCUZNFnT5arrL2LQq-L0cX0wO_y4xJTuymYTT1FCA-lb3pPpfJEpMvPuwTs2RGb_q4QuG3ZFKBAdzAZ7z58y0Uy8GtS-fJqcv0pPOiLGksdxDjhJYFttODlqpGVXpyv95M0aiip7KhyER8PORNwkncjvHpnmVHC_TGta9begZFilvNqZm0DSjwlCl9bl2h_9FQaRXycDkJbVqQ",
						},
					},
					_hostconfig: {
						type: "folder",
						id: "_hostconfig",
						name: "_hostconfig",
						content: {
							"_hostconfig/pvr": {
								type: "folder",
								id: "_hostconfig/pvr",
								name: "pvr",
								content: {
									"_hostconfig/pvr/docker.json": {
										type: "json",
										id: "_hostconfig/pvr/docker.json",
										name: "docker.json",
										content: {
											platforms: [
												"linux/arm64",
												"linux/arm",
											],
										},
									},
								},
							},
						},
					},
				},
			},
			"pvr-sdk": {
				id: "_sigs/pvr-sdk.json",
				name: "pvr-sdk",
				type: TYPES.PACKAGE,
				included: new Set([
					"_sigs/pvr-sdk.json",
					"pvr-sdk/_dm/root.squashfs.json",
					"pvr-sdk/lxc.container.conf",
					"pvr-sdk/root.squashfs",
					"pvr-sdk/root.squashfs.docker-digest",
					"pvr-sdk/root.squashfs.hash",
					"pvr-sdk/run.json",
					"pvr-sdk/src.json",
					"_config/pvr-sdk/etc/pvr-sdk/config.json",
				]),
				content: {
					_config: {
						id: "_config",
						name: "_config",
						type: TYPES.FOLDER,
						content: {
							"_config/pvr-sdk": {
								id: "_config/pvr-sdk",
								name: "pvr-sdk",
								type: TYPES.FOLDER,
								content: {
									"_config/pvr-sdk/etc": {
										id: "_config/pvr-sdk/etc",
										name: "etc",
										type: TYPES.FOLDER,
										content: {
											"_config/pvr-sdk/etc/pvr-sdk": {
												id: "_config/pvr-sdk/etc/pvr-sdk",
												name: "pvr-sdk",
												type: TYPES.FOLDER,
												content: {
													"_config/pvr-sdk/etc/pvr-sdk/config.json":
														{
															id: "_config/pvr-sdk/etc/pvr-sdk/config.json",
															name: "config.json",
															type: TYPES.JSON,
															content: {
																httpd: {
																	listen: "0.0.0.0",
																	port: "12368",
																},
															},
														},
												},
											},
										},
									},
								},
							},
						},
					},
					[TYPES.SIGNATURE]: {
						id: "_sigs/pvr-sdk.json",
						name: "pvr-sdk.json",
						type: TYPES.SIGNATURE,
						content: {
							alg: "RS256",
							jwk: {
								kty: "RSA",
								n: "v4IkL530XFY2OUxhoo9eQifuNlQFOf8lGnZ0RUFJoZNDQ5iGEiqyxM0Ki9XyQJUWX6DzWcyRUVORIIKoaE-rfPwaHuBgNT4WcdsF3z54lXd9MClug_z9ylH-TSKPaJjoNFP2MWC_Jyuu1lnnqnb5GsRT5XXuFP813pT9g_n_FaVl3nzwLRt9fhG8k7cS92RrGf4GZjBv3PKNFrV8poNfuh29Xrmq0Vsy-TjO5JaOd6o5EqxFepJNs-Kmf1GLf9_a-k7B7aD2AgJc-aJOFgeDV3mPPDIq_nkOxomap4kveGDcnLLp86OUdPBZ-ecdyxfhQOzern8sj8845nhfZZOFCw",
								e: "AQAB",
							},
							pvs: {
								include: ["pvr-sdk/**", "_config/pvr-sdk/**"],
								exclude: [
									"pvr-sdk/src.json",
									"_sigs/pvr-sdk.json",
								],
							},
							typ: "PVS",
							x5c: [
								"MIIDajCCAlKgAwIBAgIRAKBshe/AwP9db6uOSILSu8IwDQYJKoZIhvcNAQELBQAwHDEaMBgGA1UEAwwRUGFudGF2aXNvciBEZXYgQ0EwHhcNMjIwNjEyMTAwMDE4WhcNMjQwOTE0MTAwMDE4WjAaMRgwFgYDVQQDDA9wdi1kZXZlbG9wZXItMDEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC/giQvnfRcVjY5TGGij15CJ+42VAU5/yUadnRFQUmhk0NDmIYSKrLEzQqL1fJAlRZfoPNZzJFRU5EggqhoT6t8/Boe4GA1PhZx2wXfPniVd30wKW6D/P3KUf5NIo9omOg0U/YxYL8nK67WWeeqdvkaxFPlde4U/zXelP2D+f8VpWXefPAtG31+EbyTtxL3ZGsZ/gZmMG/c8o0WtXymg1+6Hb1euarRWzL5OM7klo53qjkSrEV6kk2z4qZ/UYt/39r6TsHtoPYCAlz5ok4WB4NXeY88Mir+eQ7GiZqniS94YNycsunzo5R08Fn55x3LF+FA7N6ufyyPzzjmeF9lk4ULAgMBAAGjgagwgaUwCQYDVR0TBAIwADAdBgNVHQ4EFgQU1e3xZhHRBJz2JJJyR16f2LG22GMwVwYDVR0jBFAwToAUWYuQ+pKC4IbpmCmvE/j4IeqRgT2hIKQeMBwxGjAYBgNVBAMMEVBhbnRhdmlzb3IgRGV2IENBghRjcQZ0eiqqtBk80eBjIASTScWJyjATBgNVHSUEDDAKBggrBgEFBQcDAzALBgNVHQ8EBAMCB4AwDQYJKoZIhvcNAQELBQADggEBABW0UCzH5m7kq4Q4b/jzQK3NPy9PF8p9lh8Ks5dvHIAQbLmMKyFbXQirnXqeeeHX9+MTE6L7mJUZ6EZobe1jiJ6TG3prq83WrQHEhBPWY95aldPAnXBxHOKz9o/qa3+eUYXuhMZ+PPbYdcmZoe82A77JVsG1JFceiDlXVzb8/+jumgx0U3sXW7aAr1ZWaM4VjZLtlfwBOJ/QCM8ltRaR2Oz7mTjfU53ZX3Q8sVATGeUEhGlSzHHTB4/oMgFvwahOifSy2nRME/Ay/CjL3/H21E0GDp/iBoAgPo7/AhMdE9iF2WgtcAwEUEpvjvWI4Yk7Ybm33PItThl7CCNco4uNO9o=",
							],
						} as Signature,
						raw: {
							"#spec": "pvs@2",
							protected:
								"eyJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJuIjoidjRJa0w1MzBYRlkyT1V4aG9vOWVRaWZ1TmxRRk9mOGxHblowUlVGSm9aTkRRNWlHRWlxeXhNMEtpOVh5UUpVV1g2RHpXY3lSVVZPUklJS29hRS1yZlB3YUh1QmdOVDRXY2RzRjN6NTRsWGQ5TUNsdWdfejl5bEgtVFNLUGFKam9ORlAyTVdDX0p5dXUxbG5ucW5iNUdzUlQ1WFh1RlA4MTNwVDlnX25fRmFWbDNuendMUnQ5ZmhHOGs3Y1M5MlJyR2Y0R1pqQnYzUEtORnJWOHBvTmZ1aDI5WHJtcTBWc3ktVGpPNUphT2Q2bzVFcXhGZXBKTnMtS21mMUdMZjlfYS1rN0I3YUQyQWdKYy1hSk9GZ2VEVjNtUFBESXFfbmtPeG9tYXA0a3ZlR0RjbkxMcDg2T1VkUEJaLWVjZHl4ZmhRT3plcm44c2o4ODQ1bmhmWlpPRkN3IiwiZSI6IkFRQUIifSwicHZzIjp7ImluY2x1ZGUiOlsicHZyLXNkay8qKiIsIl9jb25maWcvcHZyLXNkay8qKiJdLCJleGNsdWRlIjpbInB2ci1zZGsvc3JjLmpzb24iLCJfc2lncy9wdnItc2RrLmpzb24iXX0sInR5cCI6IlBWUyIsIng1YyI6WyJNSUlEYWpDQ0FsS2dBd0lCQWdJUkFLQnNoZS9Bd1A5ZGI2dU9TSUxTdThJd0RRWUpLb1pJaHZjTkFRRUxCUUF3SERFYU1CZ0dBMVVFQXd3UlVHRnVkR0YyYVhOdmNpQkVaWFlnUTBFd0hoY05Nakl3TmpFeU1UQXdNREU0V2hjTk1qUXdPVEUwTVRBd01ERTRXakFhTVJnd0ZnWURWUVFEREE5d2RpMWtaWFpsYkc5d1pYSXRNREV3Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRQy9naVF2bmZSY1ZqWTVUR0dpajE1Q0orNDJWQVU1L3lVYWRuUkZRVW1oazBORG1JWVNLckxFelFxTDFmSkFsUlpmb1BOWnpKRlJVNUVnZ3Fob1Q2dDgvQm9lNEdBMVBoWngyd1hmUG5pVmQzMHdLVzZEL1AzS1VmNU5JbzlvbU9nMFUvWXhZTDhuSzY3V1dlZXFkdmtheEZQbGRlNFUvelhlbFAyRCtmOFZwV1hlZlBBdEczMStFYnlUdHhMM1pHc1ovZ1ptTUcvYzhvMFd0WHltZzErNkhiMWV1YXJSV3pMNU9NN2tsbzUzcWprU3JFVjZrazJ6NHFaL1VZdC8zOXI2VHNIdG9QWUNBbHo1b2s0V0I0TlhlWTg4TWlyK2VRN0dpWnFuaVM5NFlOeWNzdW56bzVSMDhGbjU1eDNMRitGQTdONnVmeXlQenpqbWVGOWxrNFVMQWdNQkFBR2pnYWd3Z2FVd0NRWURWUjBUQkFJd0FEQWRCZ05WSFE0RUZnUVUxZTN4WmhIUkJKejJKSkp5UjE2ZjJMRzIyR013VndZRFZSMGpCRkF3VG9BVVdZdVErcEtDNElicG1DbXZFL2o0SWVxUmdUMmhJS1FlTUJ3eEdqQVlCZ05WQkFNTUVWQmhiblJoZG1semIzSWdSR1YySUVOQmdoUmpjUVowZWlxcXRCazgwZUJqSUFTVFNjV0p5akFUQmdOVkhTVUVEREFLQmdnckJnRUZCUWNEQXpBTEJnTlZIUThFQkFNQ0I0QXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBQlcwVUN6SDVtN2txNFE0Yi9qelFLM05QeTlQRjhwOWxoOEtzNWR2SElBUWJMbU1LeUZiWFFpcm5YcWVlZUhYOStNVEU2TDdtSlVaNkVab2JlMWppSjZURzNwcnE4M1dyUUhFaEJQV1k5NWFsZFBBblhCeEhPS3o5by9xYTMrZVVZWHVoTVorUFBiWWRjbVpvZTgyQTc3SlZzRzFKRmNlaURsWFZ6YjgvK2p1bWd4MFUzc1hXN2FBcjFaV2FNNFZqWkx0bGZ3Qk9KL1FDTThsdFJhUjJPejdtVGpmVTUzWlgzUThzVkFUR2VVRWhHbFN6SEhUQjQvb01nRnZ3YWhPaWZTeTJuUk1FL0F5L0NqTDMvSDIxRTBHRHAvaUJvQWdQbzcvQWhNZEU5aUYyV2d0Y0F3RVVFcHZqdldJNFlrN1libTMzUEl0VGhsN0NDTmNvNHVOTzlvPSJdfQ",
							signature:
								"NIviuNqrdvZPrwcqTisqW4nluYZ1yv9SapqMQINaBIY6YPeWw4OZ74PyB8z50rKsHprzNPOrMIJwYt51bEA0MJLpcJIaPZDy5pY_WURsjgEJ686l4s8aibDygm6YbiSJ4idJGXeA7TgsBPK0h0YXd0S-MAB0gbEOhFA_8UvAXV-R3brep8vrfdNAQaAHI_oQ0WkTxrJtjKXO5q7fL0Z9hjN7daHqYg9SyB1p_Gu8Vp5jATnshN2DqFr3S6VOGSSPvvnZldGniWbyROle4b_ijBNHaD67lQhDF8dPIzMX8lIhQ9lDT4BoFE-NlLMty27mylpZJHs7h4Ed1Y4DHVdV2w",
						},
					},
					"pvr-sdk": {
						id: "pvr-sdk",
						name: "pvr-sdk",
						type: TYPES.FOLDER,
						content: {
							"pvr-sdk/_dm": {
								id: "pvr-sdk/_dm",
								name: "_dm",
								type: TYPES.FOLDER,
								content: {
									"pvr-sdk/_dm/root.squashfs.json": {
										id: "pvr-sdk/_dm/root.squashfs.json",
										name: "root.squashfs.json",
										type: TYPES.JSON,
										content: {
											data_device: "root.squashfs",
											hash_device: "root.squashfs.hash",
											root_hash:
												"8f704d7548af4e0fe854057fe22a60e0f344016809a80fcffeb81b5d6df86046",
											salt: "64440fb2cd2374345715ed8fa574e5b798d97b30b8d90d79d70012f4ac97462b",
											type: "dm-verity",
											uuid: "125e28bb-90af-4d13-b72b-c14549476299",
										},
									},
								},
							},
							"pvr-sdk/lxc.container.conf": {
								id: "pvr-sdk/lxc.container.conf",
								name: "lxc.container.conf",
								type: TYPES.OBJECT,
								content:
									"a69205914de2e8b95270f94591d5c015796590da5273db35ef6b2ed40631fcca",
							},
							"pvr-sdk/root.squashfs": {
								id: "pvr-sdk/root.squashfs",
								name: "root.squashfs",
								type: TYPES.OBJECT,
								content:
									"7d6dd737065bfae3cfd90526276ef7c016d1012ad10513429147bec0f3aa3463",
							},
							"pvr-sdk/root.squashfs.docker-digest": {
								id: "pvr-sdk/root.squashfs.docker-digest",
								name: "root.squashfs.docker-digest",
								type: TYPES.OBJECT,
								content:
									"21c55831331afa7eccd762d8ae2292f459550d868f5d03a09bc1fe59f8584272",
							},
							"pvr-sdk/root.squashfs.hash": {
								id: "pvr-sdk/root.squashfs.hash",
								name: "root.squashfs.hash",
								type: TYPES.OBJECT,
								content:
									"ee0aa80e68233030c759adad5c60cf5e461c726e35c34f93cc451d6ff2dd9b80",
							},
							"pvr-sdk/run.json": {
								id: "pvr-sdk/run.json",
								name: "run.json",
								type: TYPES.JSON,
								content: {
									"#spec": "service-manifest-run@1",
									config: "lxc.container.conf",
									drivers: {
										manual: [],
										optional: [],
										required: [],
									},
									group: "platform",
									name: "pvr-sdk",
									restart_policy: "system",
									roles: ["mgmt"],
									"root-volume": "dm:root.squashfs",
									status_goal: "STARTED",
									storage: {
										"docker--etc-dropbear": {
											persistence: "permanent",
										},
										"docker--etc-volume": {
											persistence: "permanent",
										},
										"docker--home-pantavisor-.ssh": {
											persistence: "permanent",
										},
										"docker--var-pvr-sdk": {
											persistence: "permanent",
										},
										"lxc-overlay": {
											persistence: "boot",
										},
									},
									type: "lxc",
									volumes: [],
								},
							},
							"pvr-sdk/src.json": {
								id: "pvr-sdk/src.json",
								name: "src.json",
								type: TYPES.JSON,
								content: {
									"#spec": "service-manifest-src@1",
									args: {
										PV_GROUP: "platform",
										PV_LXC_EXTRA_CONF:
											"lxc.mount.entry = /volumes/_pv/addons/plymouth/text-io var/run/plymouth-io-sockets none bind,rw,optional,create=dir 0 0",
										PV_RESTART_POLICY: "system",
										PV_ROLES: ["mgmt"],
										PV_SECURITY_WITH_STORAGE: "yes",
										PV_STATUS_GOAL: "STARTED",
									},
									dm_enabled: {
										"root.squashfs": true,
									},
									docker_config: {
										ArgsEscaped: true,
										Cmd: ["/sbin/init"],
										Env: [
											"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
											"PVR_DISABLE_SELF_UPGRADE=true",
											"PVR_CONFIG_DIR=/var/pvr-sdk/.pvr",
										],
										Volumes: {
											"/etc-volume": {},
											"/etc/dropbear": {},
											"/home/pantavisor/.ssh": {},
											"/var/pvr-sdk": {},
										},
										WorkingDir: "/workspace",
									},
									docker_digest:
										"registry.gitlab.com/pantacor/pv-platforms/pvr-sdk@sha256:66269a3f5050dbcb5d15ba9926d052026713bbd9bbc37963476ccb924bc606f5",
									docker_name:
										"registry.gitlab.com/pantacor/pv-platforms/pvr-sdk",
									docker_source: "remote,local",
									docker_tag: "arm32v6",
									template: "builtin-lxc-docker",
								},
							},
						},
					},
				},
			},
		};

		const parts = CreateFsDescription(state);
		expect(expected_parts).toStrictEqual(parts);
	});
});
