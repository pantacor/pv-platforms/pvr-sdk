#!/bin/sh

set -e

if [ -n "$1" ]; then
  dir=`$1`
else 
  dir=$(sh -c "cd $(dirname $0); pwd")
fi

echo "building from the src directory: $dir"
appname="pvtx"
version=$(cat $dir/pvpkg.json | jq -r ".version")
build_version=$(git describe --long --all | sed -e "s/^.*-//g")
if ! [ -d $dir/releases ]; then
  mkdir -p $dir/releases
fi
tar czf $dir/releases/${appname}_${version}-${build_version}.tgz -C $dir/files/usr/bin/ pvtx JSON.sh

