PVR-SDK HTTP API Service
===============

## DESCRIPTION

Manage device in local network without Pantacor Hub by using pvr CLI on your development computer to push, pull and update a local device directly without using the Cloud Service.

PVR-SDK HTTP API Service configuration file path is: `/etc/pvr-sdk/config.json`

The content of the file is:
```json
{
	"httpd": {
		"listen": "127.0.0.1",
		"port": "12368"
	}
}
```

One why to activate this will be, by temporally change the content of the file and restart the HTTP server, this could be done via SSH using the root user and the device ip address.

```bash
ssh root@DEVICE_IP
```

Then inside the device:

```bash
echo "{\"httpd\":{\"listen\":\"0.0.0.0\",\"port\":\"12368\"}}" > /etc/pvr-sdk/config.json
ps | grep " -h /www" | head -n 1 | awk '{print $1}' | xargs kill -9
```

That will kill the http service and it will restart automaticly again.

After the port is open, one way to make this change more permant, we should overload the `config.json` of `pvr-sdk`.

```bash
pvr clone http://DEVICE_IP:12368/cgi-bin local_device
cd local_device
mkdir -p _config/pvr-sdk/etc/pvr-sdk
echo "{\"httpd\":{\"listen\":\"0.0.0.0\",\"port\":\"12368\"}}" > _config/pvr-sdk/etc/pvr-sdk/config.json
pvr add . && pvr commit && pvr post
```

That will switch your device autommaticly to local mode and disconnect the device from the cloud and if you want to connected to the cloud again you will need to run the `pantabox` command `go-remote`. That will push the state that the device has in the moment to the cloud and then switch over to the cloud and continue with the same state the device had when was in local mode.
