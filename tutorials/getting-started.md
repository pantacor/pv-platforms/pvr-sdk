# Getting Started with Pantavisor Linux and Pantabox

Pantavisor Linux comes installed with Pantabox where you can configure, install and update your apps and even the Pantavisor itself. In this guide we'll show you how to download the Pantavisor Linux and flash the device. We'll then describe how to download and install an app onto the device from the Pantacor One Marketplace. 

This guide contains the following sections: 

* [Download Pantavisor Linux](#download)
* [Installing Apps](#install-apps)
* [Updating Apps and firmware](#update-apps) 


## <a name="download"></a>Download Pantavisor Linux and flash your SD Card

1. Pantavisor Linux comes installed with Pantabox.  Select the correct architecture for your device and then [download a pre-compiled image](https://docs.pantahub.com/initial-images.html).

2.  Unzip the image file to get a flashable image with: 

    `unxz [name of image]`

3. Now you are ready to flash the SD card with the bootable image. 
4. Insert the card into your device and boot with the image. When complete, you should see the following: 

[insert screen capture of Pantavisor Logo and login instructions]

5. Login with `root` and password: `pantabox`. (You can later change the default logins with `pantabox-chpasswd`)

6. Display the menu with `pantabox`

[screen capture with menu]

## <a name="install-apps"></a>Installing apps to the device

1. Select `Install` where you see the following: 

[app install screen]

There are three sources that you can install apps from: 

* App Marketplace at [https://one.pantacor.com](https://one.pantacor.com) - free marketplace for Pantavisor apps.
* Docker Hub - Standard Docker images that can be installed with Pantavisor to run on any device. 
* Custom Source - Any device or device hub that is either hosted by Pantacor or not.

2. Select the Pantacor One App Marketplace. 

3. Choose the Home Assistant app and press OK.

4. Once the app is installed, type `panatabox` and select `View`. You should see the following: 

[screen capture of installed app]

## <a name="update-apps"></a>Updating apps and firmware

To update any app or firmware or even Pantavisor Linux itself: 

1. Run `pantabox-update` 
2. Choose the App or firmware to update. 

Pantavisor retrieves all of the parts, rebuilds the container and may reboot the device afterwards. 





