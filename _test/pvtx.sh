#!/bin/sh

set -e

cmd=$0
dir=$(sh -c "cd $(dirname $cmd); pwd")

result_file="$(mktemp)"
expected_file="$(mktemp)"
diffing="$(mktemp)"

export PVCONTROL=$dir/mocks/pvcontrol
export PVTXDIR=$dir/localpvtx

[ -d $PVTXDIR ] || mkdir -p $PVTXDIR
[ -d $dir/results ] || mkdir -p $dir/results

alias pvtx="$dir/../files/usr/bin/pvtx"

testCreateEmptyTransaction() {
  pvtx abort >/dev/null 2>&1 || true
  pvtx begin empty >/dev/null 2>&1

  pvtx show | jq >$result_file
  cat $dir/mocks/data/state-empty.json | jq >$expected_file

  diff $result_file $expected_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testProcessJsonKeysWithSpaces() {
  pvtx abort >/dev/null 2>&1 || true
  pvtx begin empty >/dev/null 2>&1

  pvtx add $dir/mocks/data/spaces_keys/json >/dev/null 2>&1
  pvtx show | jq >$result_file
  cat $dir/expected/state_with_spaces.json | jq >$expected_file

  diff $result_file $expected_file >$diffing

  assertEquals "" "$(cat $diffing)"
}


testSignatureRemoval() {
  pvtx abort >/dev/null 2>&1 || true
  pvtx begin >/dev/null 2>&1
  pvtx remove _sigs/awconnect.json >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$expected_file
  cat $dir/expected/state_after_remove.json | jq >$result_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testSignatureRemoval2() {
  pvtx abort >/dev/null 2>&1 || true
  pvtx begin >/dev/null 2>&1
  pvtx remove awconnect >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_after_remove.json | jq >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testSignatureRemoval3() {
  pvtx abort >/dev/null 2>&1 || true
  pvtx begin >/dev/null 2>&1
  pvtx remove _config/awconnect >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_after_remove.json | jq >$expected_file

  assertNotEquals "$expected_file" "$result_file"
}

testRemovalConfigPkg() {
  pvtx abort >/dev/null 2>&1 || true
  PVCONTROL_MOCK_STATE=state-1.json pvtx begin >/dev/null 2>&1
  pvtx remove _sigs/nginx-config.json >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_after_remove_config_pkg.json | jq >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testPackageUpdate() {
  cd $dir/mocks/data/awconnect/
  tar c json | gzip >/tmp/awconnect.tar.gz
  cd $dir
  pvtx abort >/dev/null 2>&1 || true
  pvtx begin >/dev/null 2>&1
  cat /tmp/awconnect.tar.gz | pvtx add - >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_after_adding_existing.json | jq >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testAddPackageFromTar() {
  pvtx abort >/dev/null 2>&1 || true

  PVCONTROL_MOCK_STATE=state-empty.json pvtx begin >/dev/null 2>&1

  pvtx add $dir/mocks/data/watchdog_pinger.tar >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/test_add_from_tar.json | jq >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testAddNewPackage() {
  cd $dir/mocks/data/vaultwarden/
  tar c json | gzip >/tmp/vaultwarden.tar.gz
  cd $dir
  pvtx abort >/dev/null 2>&1 || true
  pvtx begin >/dev/null 2>&1
  pvtx add /tmp/vaultwarden.tar.gz >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_after_new_package.json | jq >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testAddNewPackageFromCat() {
  cd $dir
  pvtx abort >/dev/null 2>&1 || true
  pvtx begin >/dev/null 2>&1

  cat $dir/mocks/data/pvwificonnect.tgz | pvtx add - >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_after_pvwificonnect.json | jq >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testUpdateBsp() {
  cd $dir/mocks/data/bsp_update/
  tar c json | gzip >/tmp/bsp_update.tar.gz

  cd $dir

  pvtx abort >/dev/null 2>&1 || true

  PVCONTROL_MOCK_STATE=bsp_init/json pvtx begin >/dev/null 2>&1

  pvtx add /tmp/bsp_update.tar.gz >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_bsp_update.json | jq >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testUpdateBspWithGroups() {
  cd $dir/mocks/data/bsp_update/
  tar c json | gzip >/tmp/bsp_update.tar.gz

  cd $dir

  pvtx abort >/dev/null 2>&1 || true

  PVCONTROL_MOCK_STATE=bsp_init_groups/json pvtx begin >/dev/null 2>&1

  pvtx add /tmp/bsp_update.tar.gz >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_bsp_update.json | jq >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testInstallFromTgz() {
  cd $dir/mocks/data/os/
  tar c json | gzip >/tmp/os.tar.gz

  pvtx abort >/dev/null 2>&1 || true

  PVCONTROL_MOCK_STATE=state-empty.json pvtx begin >/dev/null 2>&1

  pvtx add /tmp/os.tar.gz >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_after_os_install.json | jq >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testTwoPackageSigningSameFiles() {
  cd $dir/mocks/data/config_b/
  tar c json | gzip >/tmp/config_b.tar.gz

  cd $dir
  pvtx abort >/dev/null 2>&1 || true

  PVCONTROL_MOCK_STATE=config_a/json pvtx begin >/dev/null 2>&1

  pvtx add /tmp/config_b.tar.gz >/dev/null 2>&1
  pvtx remove _sigs/config_a.json >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_after_install_config_b_remove_config_a.json | jq >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testTwoPackageSigningSameFilesWithGlobs() {
  cd $dir/mocks/data/config_b_glob/
  tar c json | gzip >/tmp/config_b_glob.tar.gz

  cd $dir
  pvtx abort >/dev/null 2>&1 || true

  PVCONTROL_MOCK_STATE=config_a_glob/json pvtx begin >/dev/null 2>&1

  pvtx add /tmp/config_b_glob.tar.gz >/dev/null 2>&1
  pvtx remove _sigs/config_a.json >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_after_install_config_b_remove_config_a_with_glob.json | jq >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testRemovalOfSignedConfig() {
  cd $dir/mocks/data/pvws_with_config/
  tar c json | gzip >/tmp/pvws_with_config.tar.gz

  cd $dir/mocks/data/pvws_without_config/
  tar c json | gzip >/tmp/pvws_without_config.tar.gz

  cd $dir

  pvtx abort >/dev/null 2>&1 || true

  pvtx begin $dir/mocks/data/state-empty.json >/dev/null 2>&1

  pvtx add /tmp/pvws_with_config.tar.gz >/dev/null 2>&1

  pvtx show | jq >$result_file
  cat $dir/expected/state_pvws_with_config.json | jq >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"

  pvtx add /tmp/pvws_without_config.tar.gz >/dev/null 2>&1
  pvtx commit >/dev/null 2>&1

  cat $dir/results/state.new.json | jq >$result_file
  cat $dir/expected/state_pvws_without_config.json | jq >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testQueueNew() {
  rm -rf $dir/localqueue/*
  pvtx queue new $dir/localqueue $dir/localobjects >/dev/null 2>&1
  status=$(pvtx queue status 2>&1 | head -n 6)
  result=$(
    cat <<EOF
queue folder: $dir/localqueue
objects folder: $dir/localobjects

queue:

objects:
EOF
  )
  assertEquals "$result" "$status"
}

testQueueActions() {
  pvtx queue new $dir/localqueue $dir/localobjects >/dev/null 2>&1
  pvtx queue remove "nginx"
  pvtx queue remove "_config/nginx"
  pvtx queue remove "_sigs/nginx-config.json"
  pvtx queue unpack $dir/mocks/data/pvwificonnect.tgz

  result=$(pvtx queue status 2>&1 | head -n 9)
  expected=$(
    cat <<EOF
queue folder: $dir/localqueue
objects folder: $dir/localobjects

queue:
001__nginx.remove
002___config%2Fnginx.remove
003___sigs%2Fnginx-config.json.remove
004__pvwificonnect/json
EOF
  )

  assertEquals "$expected" "$result"
}

# testQueueProcess()  {
#   pvtx abort || true

#   pvtx queue new $dir/localqueue $dir/localobjects >/dev/null 2>&1
#   pvtx queue remove "_sigs/nginx.json"

#   # Download directly from gitlab
#   # pvwificonnect-v1.0.0-g82ca1d7.arm32v6.tgz
#   curl --silent --location "https://gitlab.com/pantacor/pvwificonnect/-/package_files/129853072/download" | pvtx queue unpack -

#   pvtx begin $dir/mocks/data/state-1.json >/dev/null 2>&1

#   pvtx queue process >/dev/null 2>&1

#   pvtx show | jq >$result_file
#   cat $dir/expected/test_queue_process.json | jq >$expected_file

#   diff $expected_file $result_file >$diffing

#   assertEquals "" "$(cat $diffing)"
# }

testDeploy() {
  pvtx abort || true

  rm -rf $dir/localrevisions/001 || true
  cp -r $dir/localrevisions/0 $dir/localrevisions/001

  pvtx queue new $dir/localqueue $dir/localobjects >/dev/null 2>&1

  pvtx queue unpack $dir/mocks/data/pvwificonnect.tgz >/dev/null 2>&1

  pvtx begin $dir/localrevisions/001/.pvr/json $dir/localobjects >/dev/null 2>&1

  pvtx queue process >/dev/null 2>&1
  pvtx show | jq >$expected_file
  pvtx deploy $dir/localrevisions/001 >/dev/null 2>&1

  cat $dir/localrevisions/001/.pvr/json | jq >$result_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testDeployDifferentFolder() {
  pvtx abort || true

  pvtx queue new $dir/localqueue $dir/localobjects >/dev/null 2>&1

  pvtx queue unpack $dir/mocks/data/pvwificonnect.tgz >/dev/null 2>&1

  PVCONTROL_MOCK_STATE=../../localrevisions/0/.pvr/json pvtx begin >/dev/null 2>&1

  pvtx queue process $dir/localqueue >/dev/null 2>&1
  pvtx deploy $dir/localrevisions/1 $dir/localobjects >/dev/null 2>&1

  src=$(sha256sum $dir/localrevisions/1/.pv/pv-initrd.img | awk '{print $1}')
  obj_link=$(sha256sum $dir/localrevisions/1/bsp/pantavisor | awk '{print $1}')
  assertEquals "pv-initrd.img is a symbol link of $src" "$obj_link" "$src"

  src=$(sha256sum $dir/localrevisions/1/.pv/pv-kernel.img | awk '{print $1}')
  obj_link=$(sha256sum $dir/localrevisions/1/bsp/kernel.img | awk '{print $1}')
  assertEquals "pv-kernel.img is a symbol link of $src" "$obj_link" "$src"

  rm -rf $dir/localrevisions/1 >/dev/null 2>&1
}

testProcessQueueWithoutbegin() {
  pvtx queue new $dir/localqueue $dir/localobjects >/dev/null 2>&1
  pvtx queue unpack $dir/mocks/data/watchdog_pinger.tar >/dev/null 2>&1
  pvtx queue process $dir/localrevisions/0/ >/dev/null 2>&1
  pvtx show | jq >$expected_file
  pvtx deploy $dir/localrevisions/1 >/dev/null 2>&1

  cat $dir/localrevisions/1/.pvr/json | jq >$result_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"

  assertEquals "{\"ObjectsDir\": \"../../localobjects\"}" "$(cat $dir/localrevisions/1/.pvr/config)"
}

testLocalTransaction() {
  pvtx abort >/dev/null 2>&1 || true

  pvtx begin $dir/localrevisions/0/ $dir/localobjects >/dev/null 2>&1

  pvtx remove pvwificonnect >/dev/null 2>&1
  pvtx add $dir/mocks/data/watchdog_pinger.tar >/dev/null 2>&1
  pvtx show | jq >$result_file
  cat $dir/expected/test_local_transaction.json | jq >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"

  message="$(pvtx commit 2>&1)"
  err=$?
  if [ $err -ne 0 ]; then
    assertEquals "ERROR: this is a local transaction, only can be deployed with pvtx deploy" "$message"
  fi
}

testProcessManualQueue() {
  rm -rf $dir/localqueue || true
  mkdir -p $dir/localqueue
  mkdir -p $dir/localobjects

  pvtx abort || true
  cp $dir/mocks/data/pvwificonnect.tgz $dir/localqueue/001__pvwificonnect.tgz
  pvtx queue process $dir/localrevisions/0 $dir/localqueue $dir/localobjects >/dev/null 2>&1
  pvtx show | jq >$expected_file

  lines=$(cat $PVTXDIR/state.JSON.sh.1.draft | grep pvwificonnect | wc -l)
  assertEquals "7" "$lines"

  pvtx deploy $dir/localrevisions/002 >/dev/null 2>&1

  cat $dir/localrevisions/002/.pvr/json | jq >$result_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

#Load shUnit2.
. $dir/lib/shunit2
