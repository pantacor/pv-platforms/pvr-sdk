pantabox-chpasswd -- change password your system password
===================
## SYNOPSIS

`pantabox-chpasswd`

## DESCRIPTION

Pantabox uses a root user with a default password to directly login onto the device or to acccess it remotely via SSH at port:22. This command allows you to change the default password.

You'll be asked for the password and a confirmation. Your keystrokes are invisible while typing the password. The password cannot be empty and both the password and its confirmation needs to be validated as the same value. 

After those validations, an OS call of chpasswd is executed. 

## EXAMPLES

![UI asking for password](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/fd884f309dc79bd03ef21cdb4a5deed43bf13725/images/chpassword1.png)

![UI asking for password confirmation](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/fe9c2e7c1bafc13d49ac6e6a8b8c2189b494b99d/images/chpassword2.png)

![UI showing success](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/fd884f309dc79bd03ef21cdb4a5deed43bf13725/images/chpassword3.png)

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.