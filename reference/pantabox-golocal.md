pantabox-golocal -- swith your device from cloud base functionality to local
====================

## SYNOPSIS

`pantabox-golocal`

## DESCRIPTION

Switches your device from consuming user-meta and revisions from the cloud to consuming all the Pantavisor bits from local mode. 

And once you're in local mode, you will able to install, update and configure everything for Pantavisor without the a cloud service.

## EXAMPLES


## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.