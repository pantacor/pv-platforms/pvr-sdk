pvcontrol -- manage your pantavisor system with a CLI
===============

## SYNOPSIS

`pvcontrol [options] [commands] [arguments]`

## DESCRIPTION

pvcontrol is a CLI tool to interact with your Pantavisor system that will allow to monitor and manage your device.

## OPTIONS

`-h`
  Show help

`-v`
  Verbose

`-s` <path>
  Send queries to socket (default: /pantavisor/pv-ctrl)

`-f` <path>
  Send output to file instead of stdout

`-m` <message>
  Commit message (only used for steps install and steps put)

## COMMANDS

`ls`
  List all containers existing in the current revision

`groups`
  Container groups related operations

`signal`
  Send signals to Pantavisor

`commands`
  Send commands to the Pantavisor state machine

`usrmeta`
  User metadata related operations

`devmeta`
  Device metadata related operations

`buildinfo`
  Dump Pantavisor build info

`objects`
  Object related operations

`steps`
  Step related operations

`conf`
  Configuration related operations

To get more information about each command, just type `pvcontrol [command] -h`. For example, `pvcontrol ls -h`.

## RETURN VALUE

`0`
  Success

`1`
  Syntax error

`48`
  Not enough disk space available

`60`
  Object has bad checksum

`70`
  Step verification has failed

`255`
  Unknown response

Return value layout is organized by command in the following categories for future return values: pvcontrol script errors (1-15), ls (16-31), commands (32-47), objects (48-63), steps (64-79), usrmeta (80-95), devmeta (96-111), buildinfo (112-127), conditions (128-143), conf (144-159) and unknown (255).

## COPYRIGHT

pvcontrol is Copyright (c) 2022 by Pantacor Ltd.
