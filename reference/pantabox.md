pantabox -- manage your pantavisor system with a UI
===============


## SYNOPSIS

`pantabox`

## DESCRIPTION

pantabox is a UI tool to manage your pantavisor system, that will allow to run every kind of actions to  mantain your system.

You can read documentation for evey action in particular by using `man` and the command name. The commands names are:

```
pantabox-chpasswd
pantabox-claim
pantabox-edit
pantabox-edit-config
pantabox-edit-sshkeys
pantabox-edit-usermeta
pantabox-golocal
pantabox-goremote
pantabox-goto
pantabox-help
pantabox-info
pantabox-install
pantabox-make-factory
pantabox-reboot
pantabox-redeploy
pantabox-shutdown
pantabox-update
pantabox-view
```

## EXAMPLES

```
man pantabox-info
```

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.
