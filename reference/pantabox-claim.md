pantabox-claim -- claim your device in a hub compatible cloud solution
====================================================

## SYNOPSIS

`pantabox-claim`

## DESCRIPTION

This command allows you to claim the device to a Pantacor Hub compatible cloud service (the default is [https://hub.pantacor.com](https://hub.pantacor.com)).

You can only claim it if the device has never been claimed. The device also needs to be connected to Internet and in remote mode.

To check if your device is available to be claimed, look in the top right corner of the console where you should see `MODE: CLAIM` or `MODE: REMOTE`.

![how the claim mode look like](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/9a8b5547b6cddce3e3da3c85ed50ec956b5bd9d4/images/claiming00.png)

Notes:

If you started your device in local mode and you have several updates before you claimed your device run, `make-factory` to force your current revision to 0 and causes the device to go remote.  Afterwards, you will be able to claim your device.

## EXAMPLES

1. Start the claiming process with:

`pantabox-claim`

2. Login to your hub account. If you don't have an account create a new one inside the console. 

![login or create account prompt](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/9a8b5547b6cddce3e3da3c85ed50ec956b5bd9d4/images/claiming02.png)

3. If the claim process was successful, you will see `MODE: ONLINE` in the top right of the console and a success dialog.

![claiming process success](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/57e20c1e2e2dc967f72637416f729f4fef7acc40/images/claiming03.png)

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.