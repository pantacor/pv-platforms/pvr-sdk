pantabox-reboot -- reboot your pantavisor device
==================

## SYNOPSIS

`pantabox-reboot`

## DESCRIPTION

Sends a command to Pantavisor to reboot the whole system.

## EXAMPLES

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.