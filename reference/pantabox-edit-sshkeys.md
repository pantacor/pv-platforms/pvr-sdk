pantabox-edit-sshkeys -- edit your ssh keys to access the running containers
=====================

## SYNOPSIS

`pantabox-edit-sshkeys`

## DESCRIPTION

Because Pantavisor Linux runs every part of your system in containers, you need a mechanism for managing SSH connections.  `pvr-sdk` is the container that handles SSH connections for all containers running on the device. 

SSH keys are converted to user-meta data for the device which then makes them available to all running containers.

When you add or edit your SSH keys, it opens the system editor (by default this is nano). A public SSH key should be added line by line.

## EXAMPLES

`pantabox-edit-ssh` opens the system editor, nano. 

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.