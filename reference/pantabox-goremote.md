pantabox-goremote -- switch your device from local functionality to a cloud base one
=====================

## SYNOPSIS

`pantabox-goremote`

## DESCRIPTION

This switches your context from local to remote. By going remote, the device consumes and syncronizes user-meta and revisions from a cloud service.

To perform this action, you need a claimed device. If your device is not claimed, you must first run `make-factory` before you can claim your device.

## EXAMPLES

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.