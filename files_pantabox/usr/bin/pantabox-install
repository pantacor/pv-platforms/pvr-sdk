#!/bin/sh

cmd=$0
dir=`sh -c "cd $(dirname $cmd); pwd"` 

# source functions
. $dir/../lib/pantabox/functions.sh

tmpdir=`mktempp -d -t pantabox-install--XXXXXXXXX`

marketplace_sources=/root/.pvbox/marketplace_sources
markets="$(cat $marketplace_sources) add_new_source add_from_docker"
defaultmarket=`echo ${markets} | cut -d' ' -f1`
source_market=${1:-${defaultmarket}}

clear_exit_error(){
	clear
	exit 1
}

get_market() {
	market=`dialog --stdout --colors --no-tags --title "Add new source" --inputbox "PVR source device URL" 40 80`
	if [ "$market" == "" ]; then
		dialog --stdout --colors --no-tags --yesno "Your source is invalid, do you what to try again?" 40 80
		response=$?
		case $response in
			0) get_market;;
		esac
	fi
	echo "$market"
}

if [ -z "$1" ];then
	options=""
	for market in $markets; do options="$options $market $market"; done
	source_market=`dialog --stdout --colors --no-tags --menu "Select from a marketplace or add your own" 60 80 10 $options`
	if ! [ $? = 0 ]; then
		clear_exit_error
	fi

	if [ "$source_market" == "add_new_source" ]; then
		source_market=`get_market`
		if ! [ -z "$source_market" ]; then
			echo "$source_market" >> $marketplace_sources
		fi
	fi

	if [ "$source_market" == "add_from_docker" ]; then
		pantabox-edit new
		exit 0
	fi
fi

remote="$source_market"

dialog --infobox "Getting apps from $remote" 0 0

current=`pvcontrol steps get current`
remotejson=`pvr inspect $remote`

run_jsons=`echo $current  | jq -r ". | to_entries[]  |select ( .key | endswith(\"run.json\")) | .key"`
installed_apps=
for f in $run_jsons; do
  appname=${f%%/run.json}
	if [ "$appname" = "bsp" ]; then
		continue
	fi
  installed_apps="__local__$appname ${appname}(local) on $installed_apps"
done

run_jsons=`echo $remotejson  | jq -r ". | to_entries[]  |select ( .key | endswith(\"run.json\"))| .key"`
remote_apps=
for f  in $run_jsons; do
  appname=${f%%/run.json}
	if ! echo $installed_apps | grep -q [^a-zA-Z0-9]$appname[^a-zA-Z0-9]; then
  		remote_apps="__remote__${appname} ${appname}(remote) off $remote_apps"
	elif echo $installed_apps | grep -q [^a-zA-Z0-9]$appname[^a-zA-Z0-9]; then
  		remote_apps="__remote__${appname} ${appname}(update) off $remote_apps"
	fi
done                                                                                       

tags=`dialog --stdout --colors --no-tags --checklist "Select an App from the list below (Press SPACE to add/remove)" 40 80 10 $installed_apps $remote_apps`
if ! [ $? = 0 ]; then
	clear_exit_error
fi

# Processing what to do
# Three things:
#   1. Add new App (replace current one if needed)
#   2. Remove App
#   3. Update App if already installed

add_part() {
	newstate=$1
	source=$2
	part=$3

	if [ "$source" = "local" ]; then
		sh -c "cd $newstate; pvr get $(local_ph_host)/#$part; pvr checkout" 2>&1 | dialog --progressbox "Getting local parts $part" 40 80
	elif [ "$source" = "remote" ]; then
		sh -c "cd $newstate; pvr get $remote#$part; pvr checkout" 2>&1 | dialog --progressbox "Getting remote parts $part" 40 80
	fi
}
toadd=
todel=
toup=

newstate=`mktempp -d -t pvbox-install-newstate-XXXXXXX`
sh -c "cd $newstate; pvr init"

local_parts=_config,bsp,_sigs/bsp.json,_hostconfig,groups.json,device.json,disks.json
remote_parts=

# here we add all selected bits
# note: we rely on order to decide who wins in case a part
#       is selected for local and remote
for t in $tags; do
	case $t in
		__local__*)
			local_parts=$local_parts,${t##__local__},_config/${t##__local__},_sigs/${t##__local__}.json
			;;
		__remote__*)
			remote_parts=${remote_parts}${remote_parts:+,}${t##__remote__},_config/${t##__remote__},_sigs/${t##__remote__}.json
			;;
		*)
			echo "WARN: unknown tag type: $t"
			;;
	esac
done

clear 
echo local_parts: $local_parts
echo remote_parts: $remote_parts

add_part $newstate local $local_parts
add_part $newstate remote $remote_parts

install_local $newstate
run_installed
wait_for_revision $installed_rev

