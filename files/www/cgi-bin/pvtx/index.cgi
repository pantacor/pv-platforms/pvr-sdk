#!/bin/sh

cmd=$0
dir=`sh -c "cd $(dirname $cmd); pwd"`

uri=${REQUEST_URI##/cgi-bin/}

if [ "$uri" = $REQUEST_URI ]; then
	echo "Status: 500 Internal Server Error"
	echo -ne "\r\n"
	exit
fi
