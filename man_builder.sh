#!/bin/sh

rm -rf $1
mkdir -p $1

for file in ./reference/*.md; do
  filename=`basename $file`
  base=${filename%.md}
  ronn -r --pipe $file > $1/$base.1
  gzip $1/$base.1
  rm -rf $1/$base.1
done