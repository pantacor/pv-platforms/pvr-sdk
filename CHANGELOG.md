
<a name="v1.2.6"></a>
## [v1.2.6](https://gitlab.com/pantacor/pv-platforms/pvr-sdk.git/compare/v1.2.4...v1.2.6)

> 2024-10-11

### Feature

* **pvtx:** default state view should be same as when a transaction is running

### Fix

* **pvtx:** only test for signature if already exists
* **pvtx app:** better error handling for http request to pvtx api


<a name="v1.2.4"></a>
## [v1.2.4](https://gitlab.com/pantacor/pv-platforms/pvr-sdk.git/compare/v1.2.3...v1.2.4)

> 2024-08-13

### Fix

* **pvtx:** converting JSON.sh to json when state has key with spaces


<a name="v1.2.3"></a>
## [v1.2.3](https://gitlab.com/pantacor/pv-platforms/pvr-sdk.git/compare/v1.2.2...v1.2.3)

> 2024-08-12

### Fix

* **pvtx:** on add remove linebreaks from error print
* **pvtx:** correct pantavisor error handling


<a name="v1.2.2"></a>
## [v1.2.2](https://gitlab.com/pantacor/pv-platforms/pvr-sdk.git/compare/v1.2.1...v1.2.2)

> 2024-07-19

### Feature

* **pvtx:** add pvtx/gc endpoint to start gc
* **pvtx:** add run gc to pvtx api and web interface

### Fix

* **pvtx:** solve problem with state json having \n inside the values


<a name="v1.2.1"></a>
## [v1.2.1](https://gitlab.com/pantacor/pv-platforms/pvr-sdk.git/compare/v1.1.0...v1.2.1)

> 2024-07-18

### Featue

* **pvtx:** add queue subcommands to pvtx

### Feature

* add begin from empty state
* add unified config format ls to pvcontrol
* **pvtx:** add trace logs to pvtx operations
* **pvtx:** add support for uncompressed pvexport package
* **pvtx:** don't use echo -n and touch commands
* **pvtx:** queue process can process a queue created manually outside pvtx
* **pvtx api:** use correct linebreak for HTTP responses and end response correctly

### Fix

* **pvtx:** after adding a export reload the state of main
* **pvtx:** fix testing for pvtx add from a pipe input
* **pvtx:** use dd instead of head to make compatible with busybox
* **pvtx:** signature removal
* **pvtx:** correct deploy saving files with extra linebreak that invalidates shasum
* **pvtx:** solve problem when using pvtx api and object file doesn't exists
* **pvtx api:** add logs endpoint to pvtx
* **pvtx api:** remove should work with post and steps ls option


<a name="v1.1.0"></a>
## v1.1.0

> 2024-05-02

### Feat

* Add man documentation to all the pantabox commands
* Install from marketplace
* **pvtx:** Create pvtx api and web app
* **pvtx-app:** show _config/PLATFORM as a removable parts

### Feature

* add build_export generic
* add config and dmc to build_exports
* **container-ci:** add pvpkg details in order to build export files
* **httpd:** only start pvr-sdk httpd server if debug shell is enabled
* **logs:** add logs endpoint documentation and logs tab to the application
* **pvtx:** create new state to filesystem description function
* **pvtx:** use all assets from local highlight and google fonts
* **pvtx:** group package by signature and not signed packages as navegable folders
* **pvtx:** add reference counting to signature paths
* **pvtx:** add test for pvtx
* **pvtx:** remove files included by signature
* **pvtx:** add pvcontrol config to stat&config page
* **pvtx:** add loading button to all actions in main page
* **pvtx:** add pvtx version for app, cli and api
* **pvtx:** add rest error when the called method is not the correct one
* **pvtx:** add multi selection on logs sources
* **pvtx:** Add fullscreen and follow bottom in the logs page
* **pvtx app:** add loading when deleting a container and manage errors on api service
* **tutorials:** add tutorial about local experience with PVR-SDK

### Fix

* upload progress wait
* pvr post --rev not working for non int strings
* **PVTX API:** on remove delete _sigs and redirect all pvtx commands error to &1
* **pantabox:** don't check for unclaimed.config on goremote
* **pantabox:** install select local parts with their _config and _sig
* **pantabox:** use JSON.sh to merge states when edit is done
* **pantabox:** on install add the platform.json for remotes parts
* **pvtx:** remove time command inside the make_json_draft
* **pvtx:** solve typo in content length less than or equal
* **pvtx:** pvtx add error handling when tar ingest fails
* **pvtx:** return errors numbers for pvtx commit and begin when they throw an error
* **pvtx:** correct signature searching on signatures cleaning
* **pvtx:** use only preref for the logsprint component
* **pvtx:** check if wrapperRef is not set yet
* **pvtx webapp:** Allow upload any type of file and wait for backend error
* **pvtx-app:** Use words as diffing method
* **pvtx-app:** Mantain loading until status response again
* **pvtx-app:** Validate progress existance before compare it.

